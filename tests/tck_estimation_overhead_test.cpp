
#define TCK_ESTIMATION_USE_TEST_CLOCK
#include "test_utils.hpp"

#include "tck/estimation/tck_estimation_overhead.hpp"
#include "tck/tck.hpp"

#include "catch2/catch_test_macros.hpp"
#include "catch2/catch_template_test_macros.hpp"

#include <cmath>

namespace
{

using namespace tck;
using namespace tck::estimation;
using namespace tck::test;

using namespace std::chrono_literals;

template <typename DurationType>
constexpr DurationType square(DurationType d)
{
    const auto d2 = d.count() * d.count();
    return DurationType{typename DurationType::rep{d2}};
}

template <typename DurationType>
DurationType squareroot(DurationType d)
{
    return DurationType{
        static_cast<typename DurationType::rep>(sqrt(d.count()))};
}

template <typename DurationType>
std::array<DurationType, 2>
getExpectedResults(DurationType testClockMean,   // NOLINT
                   DurationType testClockStddev) // NOLINT
{
    // NOTE: Given the TestClock's advance mode and the estimation algorithm
    // (outer - inner duration), we get the sum of two
    //       normally distributed samples of the TestClock's advance durations
    //       as the sample of a overhead measurement. If we regard the two
    //       samples as two independent random variables of normal distributions
    //       with same mean value mu and stddev sig, the following holds for the
    //       random variable D describing the overhead measurement: E[D]   = 2 *
    //       mu VAR[D] = 2 * sig^2 Inversely calculating the TestClock
    //       properties from the expected values would be more prone to roundoff
    //       errors (or maybe VAR[D] should be used in order to get rid of the
    //       sqrt at all).

    const auto resMean   = 2 * testClockMean;
    const auto resStddev = squareroot(2 * square(testClockStddev));

    return {resMean, resStddev};
}

int dummySampleFunction() { return -1; }

} // namespace

TEMPLATE_TEST_CASE("run_nonpositiveInput_throws", "", SingleStepTimer<>,
                   MovingAverageTimer<1024>)
{
    auto overheadEstimation = OverheadEstimation<TestType>{};

    SECTION("checkThrowsWithNegativeInput")
    {
        CHECK_THROWS(overheadEstimation.run(-2, dummySampleFunction));
    }

    SECTION("checkThrowsWithZeroInput")
    {
        CHECK_THROWS(overheadEstimation.run(0, dummySampleFunction));
    }
}

TEMPLATE_TEST_CASE("run_regularInput_properResults", "", SingleStepTimer<>,
                   MovingAverageTimer<1024>)
{
    auto overheadEstimation = OverheadEstimation<TestType>{};

    constexpr OverheadDurationType testClockMean{2100us};
    constexpr OverheadDurationType testClockStddev{10us};

    TestClock::setAdvanceMode(TestClock::AdvanceMode::AutoRandom, testClockMean,
                              testClockStddev);

    const auto [expectedMean, expectedStddev]
        = getExpectedResults(testClockMean, testClockStddev);

    SECTION("checkExpectedResults")
    {
        constexpr int nSamples = 1'000'000;

        const auto result
            = overheadEstimation.run(nSamples, dummySampleFunction);

        const auto meanAccuracy
            = std::chrono::duration_cast<OverheadDurationType>(.005
                                                               * expectedMean);
        const auto stdDevAccuracy
            = std::chrono::duration_cast<OverheadDurationType>(
                .005 * expectedStddev);

        CHECK(result.mean > (expectedMean - meanAccuracy));
        CHECK(result.mean < (expectedMean + meanAccuracy));
        CHECK(result.stddev > (expectedStddev - stdDevAccuracy));
        CHECK(result.stddev < (expectedStddev + stdDevAccuracy));
    }

    SECTION("checkNumberOfSamples")
    {
        constexpr int nSamples = 20;

        [[maybe_unused]] auto result
            = overheadEstimation.run(nSamples, dummySampleFunction);
        CHECK(overheadEstimation.getSamples().size() == 20);
    }
}
