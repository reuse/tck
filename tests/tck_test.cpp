
#define TCK_ENABLE_MEASUREMENT
#include "tck/tck.hpp"

#include "test_utils.hpp"

#include "catch2/catch_test_macros.hpp"
#include "catch2/catch_template_test_macros.hpp"
#include <catch2/matchers/catch_matchers_floating_point.hpp>

#include <chrono>
#include <ratio>
#include <type_traits>
#include <sstream>

using namespace std::chrono_literals;
using namespace Catch::Matchers;

namespace
{

using tck::test::TestClock;
using tck::test::TestUtils;

// Some duration types
using IntTestDurationDs   = std::chrono::duration<int, std::deci>;
using FloatTestDurationMs = std::chrono::duration<float, std::milli>;

// Constants
constexpr double EXPECTED_MIN_FP_PRECISION{1e-6};
constexpr double EXPECTED_TEST_CLOCK_PRECISION{1e-6};

constexpr std::string_view SST_INST_NAME{"singleStepTimerInst"};
constexpr std::string_view AVRG_INST_NAME{"averagingTimerInst"};
constexpr std::string_view MOV_AVRG_INST_NAME{"movAvrgTimerInst"};

// Helper functions
constexpr bool hasSubstring(std::string_view base, std::string_view substr)
{
    return base.find(substr) != std::string::npos;
}

} // anonymous namespace

// NOLINTBEGIN(*-magic-numbers)

TEST_CASE("getClockProperties Test")
{
    constexpr tck::ClockProperties properties
        = tck::getClockProperties<TestClock>();

    static_assert(properties.isSteady,
                  "Error in 'isSteady' property of TestClock");
    static_assert(properties.precision == EXPECTED_TEST_CLOCK_PRECISION,
                  "Error in 'precision' property of TestClock");
}

TEST_CASE("convertTicks Test")
{

    static_assert(
        tck::convertTicks<IntTestDurationDs>(std::chrono::milliseconds(1s))
            == 10,
        "Error in convertTicks: Wrong result");
    static_assert(tck::convertTicks<FloatTestDurationMs>(
                      std::chrono::microseconds(4500us))
                      == 4.5F,
                  "Error in convertTicks: Wrong result");
}

TEST_CASE("SingleStepTimer Test")
{
    tck::SingleStepTimer<tck::ClockPolicy<TestClock>> singleStepTimer{};

    // Compile-time checks
    static_assert(
        std::is_same_v<
            TestClock::duration,
            tck::SingleStepTimer<tck::ClockPolicy<TestClock>>::Duration>,
        "SingleStepTimer does not have expected duration type");

    static_assert(
        std::is_same_v<TestClock::duration, decltype(singleStepTimer.toc())>,
        "SinglesStepTimer::toc() does not have expected return type");

    static_assert(
        std::is_same_v<IntTestDurationDs,
                       decltype(singleStepTimer.toc<IntTestDurationDs>())>,
        "SingleStepTimer::toc<IntTestDurationDs>() does not have expected "
        "return type");

    static_assert(
        std::is_same_v<FloatTestDurationMs,
                       decltype(singleStepTimer.toc<FloatTestDurationMs>())>,
        "SingleStepTimer::toc<FloatTestDurationMs>() does not have expected "
        "return type");

    // First iteration
    singleStepTimer.tic();

    TestClock::advance(1s);

    REQUIRE(singleStepTimer.toc() == 1s);

    // Next iteration must restart measurement
    singleStepTimer.tic();

    TestClock::advance(2min);

    REQUIRE(singleStepTimer.toc() == 2min);

    // Test toc() with non-default return type
    singleStepTimer.tic();

    TestClock::advance(4s);
    TestClock::advance(42ms);

    const auto intDuration   = singleStepTimer.toc<IntTestDurationDs>();
    const auto floatDuration = singleStepTimer.toc<FloatTestDurationMs>();

    REQUIRE(intDuration == 4s);
    REQUIRE_THAT(floatDuration.count(),
                 WithinRel(4042, EXPECTED_MIN_FP_PRECISION));
}

TEMPLATE_TEST_CASE(
    "MultipleStepTimerTest", "",
    (tck::AveragingTimer<tck::ClockPolicy<TestClock>>),
    (tck::MovingAverageTimer<128, tck::ClockPolicy<TestClock>,
                             tck::NamePolicy<MOV_AVRG_INST_NAME>>))
{
    constexpr int nClkSteps{1024};

    constexpr TestClock::duration clkStepMean{4200us};
    constexpr TestClock::duration clkStepStddev{20us};

    constexpr auto meanAccuracy
        = std::chrono::duration_cast<TestClock::duration>(.005 * clkStepMean);

    TestClock::setAdvanceMode(TestClock::AdvanceMode::AutoRandom, clkStepMean,
                              clkStepStddev);

    TestType timer;

    for (int i = 0; i < nClkSteps; ++i)
    {
        timer.tic();
        [[maybe_unused]] auto d = timer.toc();
    }

    const auto stats = timer.getStatistics();

    CHECK(stats.maxDuration > clkStepMean + 2 * clkStepStddev);
    CHECK(stats.minDuration < clkStepMean - 2 * clkStepStddev);
    CHECK(stats.meanDuration > clkStepMean - meanAccuracy);
    CHECK(stats.meanDuration < clkStepMean + meanAccuracy);

    TestClock::reset();
}

TEMPLATE_TEST_CASE(
    "GetLatestDuration", "", (tck::AveragingTimer<tck::ClockPolicy<TestClock>>),
    (tck::MovingAverageTimer<128, tck::ClockPolicy<TestClock>,
                             tck::NamePolicy<MOV_AVRG_INST_NAME>>))
{
    TestType timer;

    timer.tic();
    TestClock::advance(1234ms);
    timer.toc();

    const auto d = timer.template getLatestDuration<IntTestDurationDs>();

    static_assert(
        std::is_same_v<IntTestDurationDs, std::remove_const_t<decltype(d)>>);

    CHECK(d.count() == 12);
}

TEMPLATE_TEST_CASE(
    "CastOutputDurationOnToc", "",
    tck::SingleStepTimer<tck::ClockPolicy<TestClock>>,
    tck::AveragingTimer<tck::ClockPolicy<TestClock>>,
    (tck::MovingAverageTimer<128, tck::ClockPolicy<TestClock>,
                             tck::NamePolicy<MOV_AVRG_INST_NAME>>))
{
    TestType timer;
    static_assert(
        std::is_same_v<FloatTestDurationMs,
                       decltype(timer.template toc<FloatTestDurationMs>())>,
        "toc<FloatTestDurationMs>() does not have expected return type");
}

TEST_CASE("logDurationToStdout")
{
    tck::SingleStepTimer<tck::ClockPolicy<TestClock>> testTimer;

    testTimer.tic();
    TestClock::advance(1s);
    testTimer.toc();

    tck::log(testTimer);

    testTimer.tic();
    TestClock::advance(2s);
    testTimer.toc();

    tck::log<std::chrono::seconds>(testTimer);
}

TEST_CASE("logSingleStepTimerWithStringStream")
{
    std::stringstream logStream;

    static constexpr std::string_view TEST_TIMER_NAME{"TestTimer"};
    tck::SingleStepTimer<tck::ClockPolicy<TestClock>,
                         tck::NamePolicy<TEST_TIMER_NAME>>
        testTimer;

    SECTION("logWithUserDefinedDuration")
    {
        testTimer.tic();
        TestClock::advance(1s);
        testTimer.toc();

        tck::log<std::chrono::seconds>(testTimer, logStream);

        REQUIRE(hasSubstring(logStream.str(), "TestTimer: 1s"));
    }

    SECTION("logWithTimerDuration")
    {
        testTimer.tic();
        TestClock::advance(10us);
        testTimer.toc();

        tck::log(testTimer, logStream);

        REQUIRE(hasSubstring(logStream.str(), "TestTimer: 10us"));
    }
}

TEST_CASE("logMultipleStepTimerWithStringStream")
{
    std::stringstream logStream;

    static constexpr std::string_view TEST_TIMER_NAME{"TestTimer"};
    tck::MovingAverageTimer<3, tck::ClockPolicy<TestClock>,
                            tck::NamePolicy<TEST_TIMER_NAME>>
        testTimer;

    SECTION("logDurationAndStatsWithUserDefinedDuration")
    {
        for (const auto d : {10s, 31s, 14s})
        {
            testTimer.tic();
            TestClock::advance(d);
            testTimer.toc();
        }

        tck::log<std::chrono::seconds>(testTimer, logStream);

        REQUIRE(hasSubstring(
            logStream.str(),
            "TestTimer: 14s, mean = 18s, [min, max] = [10s, 31s]"));
    }

    SECTION("logDurationAndStatsWithTimerDuration")
    {
        for (const auto d : {12us, 23us, 34us})
        {
            testTimer.tic();
            TestClock::advance(d);
            testTimer.toc();
        }

        tck::log(testTimer, logStream);

        REQUIRE(hasSubstring(
            logStream.str(),
            "TestTimer: 34us, mean = 23us, [min, max] = [12us, 34us]"));
    }
}

TEST_CASE("logWithCustomLoggerAsFunctionArg")
{
    tck::AveragingTimer<tck::ClockPolicy<TestClock>> testTimer;

    SECTION("Log current sample with unit seconds")
    {
        testTimer.tic();
        TestClock::advance(1s);
        testTimer.toc();

        auto sample = 0s;
        tck::log(testTimer,
                 [&sample](const decltype(sample) d) { sample = d; });

        REQUIRE(sample == 1s);
    }

    SECTION("Log current sample with timer's duration unit")
    {
        testTimer.tic();
        TestClock::advance(10us);
        testTimer.toc();

        decltype(testTimer)::Duration sample{};
        tck::log(testTimer,
                 [&sample](const decltype(sample) d) { sample = d; });

        REQUIRE(sample == 10us);
    }

    SECTION("Log statistics with floating point ms duration")
    {
        struct
        {
            float mean{.0f};
            float min{.0f};
            float max{.0f};
        } results;

        for (const auto d : {1ms, 3ms, 1ms})
        {
            testTimer.tic();
            TestClock::advance(d);
            testTimer.toc();
        }

        tck::log(testTimer,
                 [&results](tck::Statistics<FloatTestDurationMs> const &stats)
                 {
                     results.mean = stats.meanDuration.count();
                     results.min  = stats.minDuration.count();
                     results.max  = stats.maxDuration.count();
                 });

        // TestTimer's accuracy is 1us, hence 1e-3 as range
        CHECK_THAT(results.mean, WithinRel(1.6667, 1.e-3));
        CHECK_THAT(results.min, WithinRel(1., EXPECTED_MIN_FP_PRECISION));
        CHECK_THAT(results.max, WithinRel(3., EXPECTED_MIN_FP_PRECISION));
    }

    SECTION("Log current duration andstatistics as floating point ms")
    {
        struct
        {
            float curr{.0f};
            float mean{.0f};
            float min{.0f};
            float max{.0f};
        } results;

        for (const auto d : {2ms, 4ms, 7ms})
        {
            testTimer.tic();
            TestClock::advance(d);
            testTimer.toc();
        }

        tck::log(testTimer,
                 [&results](const FloatTestDurationMs                   d,
                            tck::Statistics<FloatTestDurationMs> const &stats)
                 {
                     results.curr = d.count();
                     results.mean = stats.meanDuration.count();
                     results.min  = stats.minDuration.count();
                     results.max  = stats.maxDuration.count();
                 });

        // TestTimer's accuracy is 1us, hence 1e-3 as range
        CHECK_THAT(results.curr, WithinRel(7., EXPECTED_MIN_FP_PRECISION));
        CHECK_THAT(results.mean, WithinRel(4.3333, 1.e-3));
        CHECK_THAT(results.min, WithinRel(2., EXPECTED_MIN_FP_PRECISION));
        CHECK_THAT(results.max, WithinRel(7., EXPECTED_MIN_FP_PRECISION));
    }
}

TEST_CASE("MacroBasedInterfaceWithSingleStepTimer")
{
    using namespace std::literals;

    TCK_INIT_TIMER(tck::SingleStepTimer, testTimer,
                   tck::ClockPolicy<TestClock>);

    static_assert(std::is_same_v<decltype(testTimer)::Clock, TestClock>,
                  "TCK_INIT_TIMER declared timer with incorrect clock type");

    TCK_SAMPLE_BEGIN(testTimer);
    TestClock::advance(1s);
    const auto sample1 = TCK_SAMPLE_END(testTimer);

    REQUIRE(sample1 == 1s);

    TCK_SAMPLE_BEGIN(testTimer);
    TestClock::advance(10ms);
    const auto sample2 = TCK_SAMPLE_END(testTimer, std::chrono::milliseconds);

    static_assert(std::is_same_v<std::chrono::milliseconds,
                                 std::remove_const_t<decltype(sample2)>>);
    REQUIRE(sample2 == 10ms);

    // Addtional call to TCK_SAMPLE_END must prolong returned result
    TestClock::advance(6ms);
    const auto sample3 = TCK_SAMPLE_END(testTimer, std::chrono::milliseconds);
    REQUIRE(sample3 == 16ms);
}

TEMPLATE_TEST_CASE(
    "MacroBasedTimerInit", "",
    tck::SingleStepTimer<tck::ClockPolicy<TestClock>>,
    tck::AveragingTimer<tck::ClockPolicy<TestClock>>,
    (tck::MovingAverageTimer<128, tck::ClockPolicy<TestClock>,
                             tck::NamePolicy<MOV_AVRG_INST_NAME>>))
{
    // Avoid TCK_INIT_TIMER here to be compatible with TEMPLATE_TEST_CASE
    TestType testTimer;

    // Just to avoid unused-variable warning
    TCK_SAMPLE_BEGIN(testTimer);
    TCK_SAMPLE_END(testTimer);
}

TEMPLATE_TEST_CASE(
    "MacroBasedLogTimerState", "",
    tck::SingleStepTimer<tck::ClockPolicy<TestClock>>,
    (tck::AveragingTimer<tck::ClockPolicy<TestClock>,
                         tck::NamePolicy<AVRG_INST_NAME>>),
    (tck::MovingAverageTimer<3, tck::ClockPolicy<TestClock>,
                             tck::NamePolicy<MOV_AVRG_INST_NAME>>))
{
    TestClock::reset();

    // Avoid TCK_INIT_TIMER here to be compatible with TEMPLATE_TEST_CASE
    TestType      testTimer;
    constexpr int unusedDummy{0};

    SECTION("logDurationToStdout")
    {
        TCK_SAMPLE_BEGIN(testTimer);
        TestClock::advance(36us);
        TCK_SAMPLE_END(testTimer);

        TCK_LOG_TIMER_STATE(testTimer);
    }

    SECTION("logToOutputStream")
    {
        std::stringstream logStream;

        for (const auto d : {27us, 4us, 44us})
        {
            testTimer.tic();
            TestClock::advance(d);
            testTimer.toc();
        }

        TCK_LOG_TIMER_STATE(testTimer, logStream);

        INFO("Checking logged string '" << logStream.str() << "'...");

        REQUIRE(hasSubstring(logStream.str(), " 44us"));
        TestUtils::CheckConditionally<tck::detail::HasStatistics<TestType>>(
            unusedDummy,
            [s = logStream.str()](const auto /*unused*/) {
                return hasSubstring(s, "mean = 25us, [min, max] = [4us, 44us]");
            });
    }

    SECTION("logWithCallableArg")
    {
        using SampleType = std::chrono::microseconds;

        struct
        {
            long curr{0l};
            long mean{0l};
            long min{0l};
            long max{0l};
        } results;

        for (const auto d : {2073ms, 2345ms, 1997ms})
        {
            testTimer.tic();
            TestClock::advance(d);
            testTimer.toc();
        }

        SECTION("Check access to latest sample")
        {
            auto sample = 0us;
            TCK_LOG_TIMER_STATE(testTimer, [&sample](const decltype(sample) d)
                                { sample = d; });
            REQUIRE(sample == 1'997'000us);
        }

        SECTION("Check access to statistics")
        {
            TestUtils::CheckConditionally<tck::detail::HasStatistics<TestType>>(
                unusedDummy,
                [&testTimer, &results](const auto /*unused*/)
                {
                    std::stringstream logStream;
                    TCK_LOG_TIMER_STATE(testTimer, logStream);
                    UNSCOPED_INFO("Actual timer state: " << logStream.str());

                    TCK_LOG_TIMER_STATE(
                        testTimer,
                        [&results](tck::Statistics<SampleType> const &s)
                        {
                            results.mean = s.meanDuration.count();
                            results.min  = s.minDuration.count();
                            results.max  = s.maxDuration.count();
                        });

                    return results.mean == 2'138'333l
                           && results.min == 1'997'000l
                           && results.max == 2'345'000l;
                });
        }

        SECTION("Check access to latest sample and statistics")
        {
            TestUtils::CheckConditionally<tck::detail::HasStatistics<TestType>>(
                unusedDummy,
                [&testTimer, &results](const auto /*unused*/)
                {
                    std::stringstream logStream;
                    TCK_LOG_TIMER_STATE(testTimer, logStream);
                    UNSCOPED_INFO("Actual timer state: " << logStream.str());

                    TCK_LOG_TIMER_STATE(
                        testTimer,
                        [&results](const SampleType                   d,
                                   tck::Statistics<SampleType> const &s)
                        {
                            results.curr = d.count();
                            results.mean = s.meanDuration.count();
                            results.min  = s.minDuration.count();
                            results.max  = s.maxDuration.count();
                        });

                    return results.curr == 1'997'000l
                           && results.mean == 2'138'333l
                           && results.min == 1'997'000l
                           && results.max == 2'345'000l;
                });
        }
    }
}
// NOLINTEND(*-magic-numbers)

namespace
{
using namespace tck;
using namespace std::literals;

static_assert(std::is_same_v<typename detail::FilterTypeList<
                                 std::is_integral, int, float, double>::Type,
                             int>,
              "FilterTypeList failed to select type int");

static_assert(std::is_same_v<typename detail::FilterTypeList<
                                 detail::IsChronoDuration, std::chrono::seconds,
                                 std::chrono::milliseconds>::Type,
                             std::chrono::seconds>,
              "FilterTypeList failed to select type std::chrono::seconds");

static_assert(
    std::is_same_v<typename detail::FilterTypeList<std::is_integral>::Type,
                   detail::EmptyType>,
    "FilterTypeList failed to handle empty type list");

static_assert(
    std::is_same_v<detail::FilterTypeList<std::is_class, char, int>::Type,
                   detail::EmptyType>,
    "FilterTypeList failed to handle not found type");

static_assert(detail::TypeListIncludes<float, char, float, int>::value,
              "detail::TypeListIncludes failed to find float element");

static_assert(!detail::TypeListIncludes<double, char, float, int>::value,
              "detail::TypeListIncludes failed to handle not included element");

static_assert(detail::IsTypeListUnique<double, char, float, int>::value,
              "detail::TypeListUnique returned false for unique list");

static_assert(
    detail::IsTypeListUnique<double>::value,
    "detail::TypeListUnique returned false for list with only one type");

static_assert(detail::IsTypeListUnique<double>::value,
              "detail::TypeListUnique returned false for empty list");

static_assert(!detail::IsTypeListUnique<double, double, int>::value,
              "detail::TypeListUnique returned true for non-unique list");

/////////// Assertions for TimerPolicyList ////////////////////////////////////

using PolicyList1 = detail::TimerPolicyList<ClockPolicy<TestClock>,
                                            NamePolicy<AVRG_INST_NAME>>;
using PolicyList2 = detail::TimerPolicyList<NamePolicy<AVRG_INST_NAME>,
                                            ClockPolicy<TestClock>>;
using PolicyList3 = detail::TimerPolicyList<NamePolicy<AVRG_INST_NAME>>;
using PolicyList4 = detail::TimerPolicyList<ClockPolicy<TestClock>>;
using PolicyList5 = detail::TimerPolicyList<>;

static_assert(std::is_same_v<typename PolicyList1::ClockPolicyType,
                             ClockPolicy<TestClock>>,
              "detail::TimerPolicyList has unexpected ClockPolicy");
static_assert(
    std::is_same_v<PolicyList1::NamePolicyType, NamePolicy<AVRG_INST_NAME>>,
    "detail::TimerPolicyList has unexpected NamePolicy");

static_assert(std::is_same_v<typename PolicyList2::ClockPolicyType,
                             ClockPolicy<TestClock>>,
              "detail::TimerPolicyList has unexpected ClockPolicy");
static_assert(
    std::is_same_v<PolicyList2::NamePolicyType, NamePolicy<AVRG_INST_NAME>>,
    "detail::TimerPolicyList has unexpected NamePolicy");

static_assert(std::is_same_v<typename PolicyList3::ClockPolicyType,
                             ClockPolicy<std::chrono::steady_clock>>,
              "detail::TimerPolicyList has unexpected ClockPolicy");
static_assert(
    std::is_same_v<PolicyList3::NamePolicyType, NamePolicy<AVRG_INST_NAME>>,
    "detail::TimerPolicyList has unexpected NamePolicy");

static_assert(std::is_same_v<typename PolicyList4::ClockPolicyType,
                             ClockPolicy<TestClock>>,
              "detail::TimerPolicyList has unexpected ClockPolicy");
static_assert(std::is_same_v<PolicyList4::NamePolicyType, NamePolicy<>>,
              "detail::TimerPolicyList has unexpected NamePolicy");

static_assert(std::is_same_v<typename PolicyList5::ClockPolicyType,
                             ClockPolicy<std::chrono::steady_clock>>,
              "detail::TimerPolicyList has unexpected ClockPolicy");
static_assert(std::is_same_v<PolicyList5::NamePolicyType, NamePolicy<>>,
              "detail::TimerPolicyList has unexpected NamePolicy");

/////////// Assertions for TCK_INIT_TIMER /////////////////////////////////////

// NOLINTBEGIN(*-magic-numbers, *-avoid-non-const-global-variables)
TCK_INIT_TIMER(SingleStepTimer, singleStepTimer1);
static_assert(decltype(singleStepTimer1)::getName() == "singleStepTimer1"sv);

TCK_INIT_TIMER(SingleStepTimer, singleStepTimer2, ClockPolicy<TestClock>,
               NamePolicy<SST_INST_NAME>);
static_assert(decltype(singleStepTimer2)::getName() == SST_INST_NAME);

TCK_INIT_TIMER(MovingAverageTimer, movAvrgTimer1, 1024);
static_assert(
    std::is_same_v<
        decltype(movAvrgTimer1),
        MovingAverageTimer<1024, NamePolicy<__tckTimerInst_movAvrgTimer1>>>);

TCK_INIT_TIMER(MovingAverageTimer, movAvrgTimer2, 1024, ClockPolicy<TestClock>);
static_assert(std::is_same_v<
              decltype(movAvrgTimer2),
              MovingAverageTimer<1024, ClockPolicy<TestClock>,
                                 NamePolicy<__tckTimerInst_movAvrgTimer2>>>);

TCK_INIT_TIMER(MovingAverageTimer, movAvrgTimer3, 1024,
               NamePolicy<MOV_AVRG_INST_NAME>, ClockPolicy<TestClock>);
static_assert(std::is_same_v<decltype(movAvrgTimer3)::Clock, TestClock>);
static_assert(std::is_same_v<decltype(movAvrgTimer3)::NamePolicy,
                             NamePolicy<MOV_AVRG_INST_NAME>>);

TCK_INIT_TIMER(MovingAverageTimer, movAvrgTimer4, 1024, ClockPolicy<TestClock>,
               NamePolicy<MOV_AVRG_INST_NAME>);
static_assert(std::is_same_v<decltype(movAvrgTimer4)::Clock, TestClock>);
static_assert(std::is_same_v<decltype(movAvrgTimer4)::NamePolicy,
                             NamePolicy<MOV_AVRG_INST_NAME>>);

[[maybe_unused]] void testTckInitTimerInFunctionScope()
{
    TCK_INIT_TIMER(AveragingTimer, averagingTimer1, NamePolicy<AVRG_INST_NAME>,
                   ClockPolicy<TestClock>);
    static_assert(
        std::is_same_v<
            decltype(averagingTimer1),
            AveragingTimer<NamePolicy<AVRG_INST_NAME>, ClockPolicy<TestClock>,
                           NamePolicy<__tckTimerInst_averagingTimer1>>>);
}
} // unnamed namespace
// NOLINTEND(*-magic-numbers, *-avoid-non-const-global-variables)
