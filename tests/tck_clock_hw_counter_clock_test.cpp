
#define TCK_ENABLE_MEASUREMENT
#include "tck/tck.hpp"
#include "tck/clock/tck_clock_hw_counter_clock.hpp"

#include "catch2/catch_test_macros.hpp"

#include <chrono>
#include <cstdlib>
#include <iostream>
#include <thread>

namespace {

using namespace tck;
using namespace tck::clock;

using Picoseconds = std::chrono::duration<int64_t, std::pico>;
using NanosecondsDouble = std::chrono::duration<double, std::nano>;
using ReferenceClock = std::chrono::steady_clock;

constexpr int64_t HW_COUNTER_CLOCK_FREQ{
    1'000'000'000}; // Don't care, we only only evaluate ticks directly
using Clock = HwCounterClock<HW_COUNTER_CLOCK_FREQ>;

void busyWait(int iterations)
{
    while (--iterations > 0)
    {
        [[maybe_unused]] const auto _ = rand(); // NOLINT
    }
}

template <class TimerType>
void measure(TimerType &timer, int iterations)
{
    while (--iterations > 0)
    {
        TCK_SAMPLE_BEGIN(timer);
        busyWait(10);
        TCK_SAMPLE_END(timer);
    }
}

}

//FIXME For evaluation. Switch to mocks for rdtsc etc and move code below to tck-overhead
TEST_CASE("hw_counter_clock")
{
    constexpr double MAX_DIFF_MEAN{250.};

    const auto [hwCounterFreq, srcId] = HwCounterClockUtils::getHwCounterFrequency();
    
    std::cout << "HW Counter frequency = " << hwCounterFreq << " Hz (src: " << srcId << ") \n";

    TCK_INIT_TIMER(AveragingTimer, referenceTimer, ClockPolicy<ReferenceClock>);
    TCK_INIT_TIMER(AveragingTimer, testTimer, ClockPolicy<Clock>);

    measure(referenceTimer, 1'000'000);
    measure(testTimer, 1'000'000);

    if (srcId < 0)
    {
        WARN("Can't determine HW clock frequency");
        return;
    }
    
    const auto refMean = static_cast<NanosecondsDouble>(referenceTimer.getStatistics().meanDuration).count();
    const auto testMean = 10e8 * static_cast<double>(testTimer.getStatistics().meanDuration.count()) / hwCounterFreq;

    std::cout << "ref clk: mean = " << refMean << " ns\n";
    std::cout << "hw clk:  mean = " << testMean << " ns\n";

    CHECK(MAX_DIFF_MEAN - (refMean - testMean) > 0);
}
