
#include "tck/tck.hpp"
#include "test_utils.hpp"

#include "catch2/catch_test_macros.hpp"

using tck::test::TestClock;
using namespace std::chrono_literals;

namespace
{
[[maybe_unused]] TCK_INIT_TIMER(tck::SingleStepTimer, testTimer);
[[maybe_unused]] TCK_INIT_TIMER(tck::SingleStepTimer, testTimer2,
                                tck::ClockPolicy<TestClock>);

static_assert(std::is_same_v<decltype(testTimer), const tck::detail::NoTimer>,
              "Declaration by TCK_INIT_TIMER is incorrect");
static_assert(std::is_same_v<decltype(testTimer2), const tck::detail::NoTimer>,
              "Declaration by TCK_INIT_TIMER is incorrect");
}

TEST_CASE("MacroBasedInterfaceDisabled")
{
    TCK_INIT_TIMER(tck::SingleStepTimer, testTimer,
                   tck::ClockPolicy<TestClock>);

    static_assert(
        std::is_same_v<decltype(testTimer), const tck::detail::NoTimer>,
        "TCK_INIT_TIMER declared timer of incorrect type if disabled");

    TCK_SAMPLE_BEGIN(testTimer);
    TestClock::advance(3s);
    const std::chrono::seconds sample1 = TCK_SAMPLE_END(testTimer);

    REQUIRE(sample1 == 0s);

    TCK_SAMPLE_BEGIN(testTimer);
    TestClock::advance(3s);
    const auto sample = TCK_SAMPLE_END(testTimer, std::chrono::seconds);

    static_assert(std::is_same_v<std::remove_const_t<decltype(sample)>,
                                 std::chrono::seconds>);
    REQUIRE(sample == 0s);

    TCK_LOG_TIMER_STATE(testTimer);
    TCK_LOG_TIMER_STATE(testTimer,
                        [](const std::chrono::seconds /*unused*/) {});
}
