#pragma once

#include "catch2/catch_test_macros.hpp"

#include <chrono>
#include <random>
#include <ratio>

namespace tck
{
namespace test
{

struct TestUtils
{
    /** Checks if predicate is true if trait is fulfilled
     *
     * Note: Arg will usually be unused, but is kept. We can pass lambdas
     *       with auto param so that operator() is templated and will only
     *       be instanciated, if it is actually used, i.e. the trait is
     * fulfilled. This way, compilation errors from types that don't fulfill the
     * trait are prevented.
     */
    template <typename Trait, typename A, typename PredicateFcn>
    static void CheckConditionally([[maybe_unused]] A            arg,
                                   [[maybe_unused]] PredicateFcn pred)
    {
        if constexpr (Trait::value)
        {
            CHECK(pred(arg));
        }
    }
};

/** Test clock class */
class TestClock
{
public:
    enum class AdvanceMode
    {
        /** Advance by specifying explicit time steps */
        Explicit,
        /** Advance on each now() call by a random step */
        AutoRandom,
    };

    // Definitions to fulfill TrivalClock requirements
    using rep      = int64_t;
    using period   = std::micro;
    using duration = std::chrono::duration<rep, period>;

    using time_point = std::chrono::time_point<TestClock>;

    static constexpr bool is_steady{true};

    static time_point now()
    {
        const auto tp = time_point{duration{startTicks + s_currentTicks}};

        if (s_advanceMode == AdvanceMode::AutoRandom)
        {
            advance();
        }

        return tp;
    };

    // Definitions for test manipulation

    /** Init sample generator, see advance() */
    template <typename InputDuration>
    static void setAdvanceMode(AdvanceMode mode, InputDuration mean = 0,
                               InputDuration stddev = 0)
    {
        s_advanceMode = mode;

        const double m = std::chrono::duration_cast<duration>(mean).count();
        const double s = std::chrono::duration_cast<duration>(stddev).count();

        s_sampleGenerator = std::normal_distribution{m, s};
    }

    /** Advance clock by inputDuration */
    template <typename InputDuration>
    static void advance(InputDuration inputDuration)
    {
        s_currentTicks
            += std::chrono::duration_cast<duration>(inputDuration).count();
    }

    /** Advance clock by normally distributed sample */
    static void advance()
    {
        s_currentTicks += std::round<rep>(s_sampleGenerator(s_randGen));
    }

    /** Reset clock ticks and AdvanceMode */
    static void reset()
    {
        s_currentTicks = 0;

        s_advanceMode = AdvanceMode::Explicit;
        s_sampleGenerator.reset();
    };

private:
    static constexpr rep startTicks{0};
    static inline rep    s_currentTicks{0};

    static inline AdvanceMode s_advanceMode{AdvanceMode::Explicit};

    // static inline std::random_device s_randDev{};
    static inline std::mt19937             s_randGen{std::random_device{}()};
    static inline std::normal_distribution s_sampleGenerator{};
};

} // namespace test
} // namespace tck
