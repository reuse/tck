/**
 * \file tck.hpp
 * \brief Main tck utilities
 *
 * This header contains all measurement utilities and helpers.
 * It is sufficient to drop it in your project as a single file.
 */
#ifndef TCK_TIMER_H
#define TCK_TIMER_H

#include <array>
#include <chrono>
#include <functional>
#include <ratio>
#include <iostream>
#include <string_view>
#include <type_traits>


////////// Helper macros ///////////////////////////////////////////////////////

/// \cond HELPER_MACROS
#define __TCK_SELECT_INIT_TIMER_IMPL(_0, _1, _2, _3, _4, _5, impl, ...) impl
#define __TCK_SELECT_SAMPLE_END_IMPL(_0, _1, impl, ...)      impl
#define __TCK_SELECT_LOG_TIMER_STATE_IMPL(_0, _1, impl, ...) impl

#ifdef TCK_ENABLE_MEASUREMENT
#define __TCK_INIT_TIMER_IMPL_2(TimerType, timer)                              \
    static constexpr std::string_view __tckTimerInst_##timer{#timer};          \
    TimerType<tck::NamePolicy<__tckTimerInst_##timer>> timer;                  \
    static_assert(tck::detail::IsTckTimer<decltype(timer)>::value,             \
                  "Specified type is not a proper tck timer")

#define __TCK_INIT_TIMER_IMPL_3(TimerType, timer, ...)                         \
    static constexpr std::string_view __tckTimerInst_##timer{#timer};          \
    TimerType<__VA_ARGS__, tck::NamePolicy<__tckTimerInst_##timer>> timer;     \
    static_assert(tck::detail::IsTckTimer<decltype(timer)>::value,             \
                  "Specified type is not a proper tck timer")

#define __TCK_SAMPLE_END_IMPL_1(timer)      timer.toc()
#define __TCK_SAMPLE_END_IMPL_2(timer, ...) timer.toc<__VA_ARGS__>()

#define __TCK_LOG_TIMER_STATE_IMPL_1(timer)      tck::log(timer)
#define __TCK_LOG_TIMER_STATE_IMPL_2(timer, ...) tck::log(timer, __VA_ARGS__)

#else // ifndef TCK_ENABLE_MEASUREMENT

#define __TCK_INIT_TIMER_IMPL_2(TimerType, timer)                              \
    static constexpr tck::detail::NoTimer timer
#define __TCK_INIT_TIMER_IMPL_3(TimerType, timer, ...)                         \
    static constexpr tck::detail::NoTimer timer

#define __TCK_SAMPLE_END_IMPL_1(timer)                                         \
    typename std::remove_reference_t<decltype(timer)>::Duration(0)
#define __TCK_SAMPLE_END_IMPL_2(timer, ...) __VA_ARGS__(0)

#define __TCK_LOG_TIMER_STATE_IMPL_1(timer)                                    \
    do                                                                         \
    {                                                                          \
    } while (0)
#define __TCK_LOG_TIMER_STATE_IMPL_2(timer, ...)                               \
    do                                                                         \
    {                                                                          \
    } while (0)
#endif

#define __TCK_MAKE_DURATION_UNIT(ratio, unitStr)                               \
    template <>                                                                \
    struct DurationUnit<ratio>                                                 \
    {                                                                          \
        static constexpr std::string_view value = {unitStr};                   \
    }
/// \endcond

namespace tck
{

/** tck library version */
inline constexpr std::string_view TCK_VERSION = "00.02.01";

////////// Forward declarations ///////////////////////////////////////////////

template <typename ClockType>
struct ClockPolicy;

template <auto const &...>
struct NamePolicy;

template <typename DurationType>
struct Statistics;

template <typename... Policies>
class SingleStepTimer;

template <size_t WindowLength, typename... Policies>
class MovingAverageTimer;

template <typename... Policies>
class AveragingTimer;

#if __cplusplus < 202002L
/** operator<< overload std::chrono::duration
 *
 * Provided for use with C++17, where the overload is not yet in the standard
 * libary. Note that this will only be found by qualified lookup.
 */
template <typename Rep, typename Period>
std::ostream &operator<<(std::ostream                            &os,
                         const std::chrono::duration<Rep, Period> d);
#endif

/** operator<< overload for Statistics */
template <typename Rep, typename Period>
std::ostream &
operator<<(std::ostream                                         &os,
           Statistics<std::chrono::duration<Rep, Period>> const &s);

////////// Implementation details //////////////////////////////////////////////

/// \cond NAMESPACE_DETAIL
namespace detail
{

struct EmptyType
{};

template <template <typename> typename Trait, typename...>
struct FilterTypeList
{
    using Type = EmptyType;
    static constexpr bool HasType() { return false; }
};

template <template <typename> typename Trait, typename A, typename... OtherArgs>
struct FilterTypeList<Trait, A, OtherArgs...>
{
    using CurrentType = std::conditional_t<Trait<A>::value, A, EmptyType>;
    using Type
        = std::conditional_t<std::is_same_v<CurrentType, EmptyType>,
                             typename FilterTypeList<Trait, OtherArgs...>::Type,
                             A>;

    static constexpr bool HasType() { return !std::is_same_v<Type, EmptyType>; }
};

template <template <typename> typename Trait, typename A>
struct FilterTypeList<Trait, A>
{
    using Type = std::conditional_t<Trait<A>::value, A, EmptyType>;

    static constexpr bool HasType() { return !std::is_same_v<Type, EmptyType>; }
};

template <typename A, typename... Args>
struct TypeListIncludes
{
    template <typename T>
    using IsSameAsA = std::is_same<A, T>;

    static constexpr bool value
        = !std::is_same_v<typename FilterTypeList<IsSameAsA, Args...>::Type,
                          EmptyType>;
};

template <typename... Types>
struct IsTypeListUnique;

template <typename Head, typename... Tail>
struct IsTypeListUnique<Head, Tail...>
{
    static constexpr bool value = !TypeListIncludes<Head, Tail...>::value
                                  && IsTypeListUnique<Tail...>::value;
};

template <typename Head>
struct IsTypeListUnique<Head> : std::true_type
{};

template <>
struct IsTypeListUnique<> : std::true_type
{};

/** Common base for all policies */
struct TimerPolicy
{};

struct ClockPolicyBase : public TimerPolicy
{};
struct NamePolicyBase : public TimerPolicy
{};

/** List of policies of a timer
 *
 * This template selects the policies for a timer from PolicyArgs and assigns
 * defauls for policies that are not specified.
 */
template <typename... PolicyArgs>
class TimerPolicyList
{
private:
    static_assert(detail::IsTypeListUnique<PolicyArgs...>::value,
                  "Policy argument list is not unique");

    template <typename T>
    using IsNamePolicy = std::is_base_of<detail::NamePolicyBase, T>;

    template <typename T>
    using IsClockPolicy = std::is_base_of<detail::ClockPolicyBase, T>;

    // Intermediate results of filtering PolicyArgs
    using CpFilterResult = detail::FilterTypeList<IsClockPolicy, PolicyArgs...>;

    using NpFilterResult = detail::FilterTypeList<IsNamePolicy, PolicyArgs...>;

public:
    /** NamePolicy of this list */
    using NamePolicyType
        = std::conditional_t<NpFilterResult::HasType(),
                             typename NpFilterResult::Type, NamePolicy<>>;

    /** ClockPolicy of this list */
    using ClockPolicyType
        = std::conditional_t<CpFilterResult::HasType(),
                             typename CpFilterResult::Type,
                             ClockPolicy<std::chrono::steady_clock>>;
};

/** Base class for MovingWindows */
template <typename Derived, size_t WindowLength, typename BufferType,
          typename ResultType = BufferType>
class MovingWindow
{
public:
    MovingWindow()
    {
        buffer.fill(
            static_cast<typename decltype(this->buffer)::value_type>(0));
    };

    /** Perform a single step of the MovingWindow */
    ResultType step(BufferType u)
    {
        // Push new value
        this->buffer[posTail] = u;

        // Impl-defined action on the buffer
        ResultType y = static_cast<Derived *>(this)->stepImpl();

        // Update ptrs
        posTail = (posTail + 1) % WindowLength;
        posHead = (posHead + 1) % WindowLength;

        return y;
    }

    [[nodiscard]] constexpr size_t size() const { return windowLength; }

protected:
    static constexpr size_t windowLength{WindowLength};

    size_t posHead{1};
    size_t posTail{0};

    std::array<BufferType, WindowLength> buffer;
};

/** MovingSum */
template <size_t WindowLength, typename BufferType,
          typename ResultType = BufferType>
class MovingSum
: public MovingWindow<MovingSum<WindowLength, BufferType, ResultType>,
                      WindowLength, BufferType, ResultType>
{
public:
    ResultType stepImpl(void)
    {
        sum += this->buffer[this->posTail];
        ResultType res{sum};
        sum -= this->buffer[this->posHead];

        return res;
    };

private:
    ResultType sum{0};
};

/** Moving average */
template <size_t WindowLength, typename BufferType,
          typename ResultType = BufferType>
class MovingAverage
{
public:
    ResultType step(BufferType u) { return sum.step(u) / sum.size(); };

private:
    MovingSum<WindowLength, BufferType, ResultType> sum;
};

// Traits for chrono::duration
template <typename T>
struct IsChronoDuration : std::false_type
{};

template <typename R, typename P>
struct IsChronoDuration<std::chrono::duration<R, P>> : std::true_type
{};

// Traits for Statistics
template <typename T>
struct IsStatistics : std::false_type
{};

template <typename R, typename P>
struct IsStatistics<Statistics<std::chrono::duration<R, P>>> : std::true_type
{};

// Traits for tck timers
template <typename TimerType>
struct IsTckTimer : std::false_type
{};

template <typename... Policies>
struct IsTckTimer<SingleStepTimer<Policies...>> : std::true_type
{};

template <size_t WindowLength, typename... Policies>
struct IsTckTimer<MovingAverageTimer<WindowLength, Policies...>>
: std::true_type
{};

template <typename ClockType, typename... Policies>
struct IsTckTimer<AveragingTimer<ClockType, Policies...>> : std::true_type
{};

template <typename TimerType>
struct HasStatistics : std::false_type
{};

template <typename... Policies>
struct HasStatistics<SingleStepTimer<Policies...>> : std::false_type
{};

template <size_t WindowLength, typename... Policies>
struct HasStatistics<MovingAverageTimer<WindowLength, Policies...>>
: std::true_type
{};

template <typename ClockType, typename... Policies>
struct HasStatistics<AveragingTimer<ClockType, Policies...>> : std::true_type
{};

struct NoTimer
{
    using Duration = std::chrono::hours;
};

template <typename T>
struct DependantFalse : std::false_type
{};

#if __cplusplus < 202002L
template <typename Period>
struct DurationUnit
{
    static_assert(DependantFalse<Period>::value,
                  "tck does not provide operator<< for this duration type");
};

__TCK_MAKE_DURATION_UNIT(std::nano, "ns");
__TCK_MAKE_DURATION_UNIT(std::micro, "us");
__TCK_MAKE_DURATION_UNIT(std::milli, "ms");
__TCK_MAKE_DURATION_UNIT(std::ratio<1>, "s");
__TCK_MAKE_DURATION_UNIT(std::ratio<3600>, "h");
__TCK_MAKE_DURATION_UNIT(std::ratio<86400>, "d");
#endif

struct ConvertibleToAnyType
{
    template <typename T>
    operator T &();

    template <typename T>
    operator T &&();
};

template <typename LoggerType>
struct LoggerCallableProperties
{
    static_assert(detail::DependantFalse<LoggerType>::value,
                  "Invalid LoggerType");
};

/** Helper class to get type properties of the callable passed to log */
template <typename R, typename... Args>
class LoggerCallableProperties<std::function<R(Args...)>>
{
    // Helpers
    template <typename T>
    using remove_cv_and_ref_t = std::remove_cv_t<std::remove_reference_t<T>>;

    template <typename T>
    using IsDuration = IsChronoDuration<remove_cv_and_ref_t<T>>;

    template <typename T>
    using IsStats = IsStatistics<remove_cv_and_ref_t<T>>;

public:
    // Return and argument types of the callable
    using ReturnType            = R;
    using SampleDurationArgType = remove_cv_and_ref_t<
        typename FilterTypeList<IsDuration, Args...>::Type>;
    using StatisticsArgType
        = remove_cv_and_ref_t<typename FilterTypeList<IsStats, Args...>::Type>;

    // Constants reflecting callable arguments
    static constexpr bool hasSampleDurationArg
        = !std::is_same_v<SampleDurationArgType, EmptyType>;
    static constexpr bool hasStatisticsArg
        = !std::is_same_v<StatisticsArgType, EmptyType>;

    // Verification of properties
    static_assert(hasSampleDurationArg || hasStatisticsArg,
                  "Invalid argument type(s) in callable passed to tck::log");
};

/** Base class with common functionality of stream loggers */
template <typename TimerType,
          typename LoggedDurationType = typename TimerType::Duration>
class StreamLoggerBase
{
public:
    explicit StreamLoggerBase(std::ostream &os)
    : m_ostream{os}
    {}

protected:
    void writePrefix()
    {
        if constexpr (TimerType::NamePolicy::isNamed)
        {
            m_ostream << TimerType::getName() << ": ";
        }
        else
        {
            m_ostream << "Latest duration = ";
        }
    }

    std::ostream &m_ostream;
};

/** Logger writing to an output stream */
template <typename TimerType,
          typename LoggedDurationType = typename TimerType::Duration,
          bool                        = HasStatistics<TimerType>::value>
class StreamLogger
{};

/** Logger writing to an output stream
 *
 * Specialization for Timers without Statistics.
 */
template <typename TimerType, typename LoggedDurationType>
class StreamLogger<TimerType, LoggedDurationType, false>
: public StreamLoggerBase<TimerType, LoggedDurationType>
{
public:
    using StreamLoggerBase<TimerType, LoggedDurationType>::StreamLoggerBase;

    /** Logs the latest sampled duration */
    void operator()(const LoggedDurationType d)
    {
        this->writePrefix();
        this->m_ostream << d << '\n';
    }
};

/** Logger writing to an output stream
 *
 * Specialization for Timers with Statistics.
 */
template <typename TimerType, typename LoggedDurationType>
class StreamLogger<TimerType, LoggedDurationType, true>
: public StreamLoggerBase<TimerType, LoggedDurationType>
{
public:
    using StreamLoggerBase<TimerType, LoggedDurationType>::StreamLoggerBase;

    /** Logs the latest sampled duration and statistics */
    void operator()(const LoggedDurationType              d,
                    Statistics<LoggedDurationType> const &s)
    {
        this->writePrefix();
        this->m_ostream << d << ", " << s << '\n';
    }
};

} // namespace detail
/// \endcond


////////// Macro-based interface ///////////////////////////////////////////////

/**
 * \defgroup macro_interface_group Macro-based interface
 * @{
 *
 * Macro-based interface of the tck library
 *
 * The macro-based interface is provided in order to facilitate timer
 * declaration and logging and to make timing code be easily distinguishable
 * from actual implementation code.
 *
 * The macro definitions are empty by default and have to be enabled at compile-time
 * by defining TCK_ENABLE_MEASUREMENT.
 */

/** Initialize timer instance
 *
 *  \param TimerType Type of the timer
 *  \param timer     Name of the instance
 *  \param Policies... List of policies, see \ref policy_group (optional)
 */
#define TCK_INIT_TIMER(...)                                                    \
    __TCK_SELECT_INIT_TIMER_IMPL(                                              \
        __VA_ARGS__, __TCK_INIT_TIMER_IMPL_3, __TCK_INIT_TIMER_IMPL_3,         \
        __TCK_INIT_TIMER_IMPL_3, __TCK_INIT_TIMER_IMPL_3,                      \
        __TCK_INIT_TIMER_IMPL_2, _5, _4, _3, _2, _1, _0)                       \
    (__VA_ARGS__)

#ifdef TCK_ENABLE_MEASUREMENT

/** Begin a sampled section
 *
 *  \param timer Timer instance
 */
#define TCK_SAMPLE_BEGIN(timer)                                                \
    do                                                                         \
    {                                                                          \
        timer.tic();                                                           \
    } while (0)
#else
#define TCK_SAMPLE_BEGIN(timer)                                                \
    do                                                                         \
    {                                                                          \
    } while (0)
#endif

/** End a sampled section
 *
 *  \param timer                Timer instance
 *  \param DurationType The type of the returned sample duration (optional)
 */
#define TCK_SAMPLE_END(...)                                                    \
    __TCK_SELECT_SAMPLE_END_IMPL(__VA_ARGS__, __TCK_SAMPLE_END_IMPL_2,         \
                                 __TCK_SAMPLE_END_IMPL_1, _0)                  \
    (__VA_ARGS__)

/** Log state of a timer instance
 *
 *  The logger argument can be a callable or std::ostream&, see tck::log.
 *
 *  \param timer  Timer instance to be logged
 *  \param logger Logging object (optional)
 */
#define TCK_LOG_TIMER_STATE(...)                                               \
    __TCK_SELECT_LOG_TIMER_STATE_IMPL(__VA_ARGS__,                             \
                                      __TCK_LOG_TIMER_STATE_IMPL_2,            \
                                      __TCK_LOG_TIMER_STATE_IMPL_1, _O)        \
    (__VA_ARGS__)

/**@}*/ // \defgroup macro_interface_group


////////// Declaration of timer policies ///////////////////////////////////////

/**
 * \defgroup policy_group Timer Policies
 * @{
 */

/** Policy defining the type of clock used for measurement
 *
 * \tparam ClockType The clock used for measurements. This type must fulfill the
 * [TrivialClock](https://en.cppreference.com/w/cpp/named_req/TrivialClock)
 * requirements of `std::chrono` clocks.
 */
template <typename ClockType>
struct ClockPolicy : public detail::ClockPolicyBase
{
    using Clock = ClockType;

protected:
    ~ClockPolicy() = default;
};

/** Policy specifying if a timer is named or unnamed
 *
 * This template argument for this policy must be either left empty for an
 * unnamed timer, or, for a named timer, reference a constexpr std::string_view
 * specifying the instance name.
 */
template <auto const &...>
struct NamePolicy
{};

/** Specialization specifying an unnamed timer */
template <>
struct NamePolicy<> : public detail::NamePolicyBase
{
    static constexpr bool isNamed{false};

protected:
    ~NamePolicy() = default;
};

/** Specialization specifying a named timer
 *
 * \tparam Instance name of the timer, should reference a constexpr
 * std::string_view
 */
template <auto const &InstanceName>
struct NamePolicy<InstanceName> : public detail::NamePolicyBase
{
    static constexpr bool isNamed{true};

    /** Gets the instance's name */
    static constexpr std::string_view getName() { return InstanceName; }

protected:
    ~NamePolicy() {};
};

/**@}*/ // \defgroup policy_group


////////// Utilities ///////////////////////////////////////////////////////////

/** Timer clock properties
 *
 * TODO Make this a trait and get rid of getClockProperties
 */
struct ClockProperties
{
    double precision{.0};
    bool   isSteady{false};
};

/** Holds the min, max and mean value of the measured duration.
 *
 * This stucture is returned by MultipleStepTimer::getStatistics.
 * The calculation of the mean value depends on the timer implementation.
 *
 * \tparam DurationType std::chrono::duration type holding the statistic values
 */
template <typename DurationType>
struct Statistics
{
    using Duration = DurationType;

    DurationType minDuration{DurationType::max().count()};
    DurationType maxDuration{DurationType::min().count()};
    DurationType meanDuration{0};
};

/** Determine ClockProperties of the ClockType
 *
 * \returns ClockProperties of the specified ClockType
 */
template <typename ClockType>
constexpr inline ClockProperties getClockProperties()
{

    double prec
        = static_cast<double>(ClockType::period::num) / ClockType::period::den;

    return {prec, ClockType::is_steady};
}

/** Utility function to convert ticks for given input and output duration types
 *
 * \tparam OutputDurationType A std::chrono::duration type the ticks should be
 * converted to \tparam InputDurationType The std::chrono::duration type of the
 * input duration
 *
 * \returns Instance of OutputDurationType::rep containing the output duration's
 * ticks
 */
template <typename OutputDurationType, typename InputDurationType>
constexpr inline auto convertTicks(const InputDurationType input)
{
    using convRatio = std::ratio_divide<typename OutputDurationType::period,
                                        typename InputDurationType::period>;

    return convRatio::den
           * static_cast<typename OutputDurationType::rep>(input.count())
           / convRatio::num;
}


/** Log state of a given timer
 *
 *  This function allows to pass a custom log operation as a callable argument.
 *  The logged duration type is deduced from the callable's argument type.
 *
 *  \param timer Timer instance to be logged
 *  \param logger Callable to perform the log operation
 *
 *  Note: The logger object may be templated on the TimerType. For timers with
 * policy::Named, this allows to use TimerType::getName to log the instance
 * name.
 */
template <class TimerType, class LoggerType,
          std::enable_if_t<!std::is_base_of_v<std::ostream, LoggerType>, bool>
          = true>
void log(TimerType const &timer, LoggerType logger)
{
    static_assert(detail::IsTckTimer<TimerType>::value,
                  "tck::log: 1st argument must be a proper tck timer");

    static_assert(
        std::is_invocable_v<LoggerType, detail::ConvertibleToAnyType>
            || std::is_invocable_v<LoggerType, detail::ConvertibleToAnyType,
                                   detail::ConvertibleToAnyType>,
        "tck::log: 2nd argument must be a callable");

    using P = detail::LoggerCallableProperties<decltype(std::function(logger))>;

    if constexpr (P::hasSampleDurationArg && P::hasStatisticsArg)
    {
        logger(timer.template getLatestDuration<
                   typename P::SampleDurationArgType>(),
               timer.template getStatistics<
                   typename P::StatisticsArgType::Duration>());
    }
    else if constexpr (P::hasSampleDurationArg)
    {
        logger(timer.template getLatestDuration<
               typename P::SampleDurationArgType>());
    }
    else if constexpr (P::hasStatisticsArg)
    {
        logger(timer.template getStatistics<
               typename P::StatisticsArgType::Duration>());
    }
    else
    {
        static_assert(detail::DependantFalse<LoggerType>::value,
                      "Error in log implementation");
    }
}

/** Log state of a given timer to an output stream
 *
 *  This overload used to log a timer's state to an output stream.
 *  By default, the StdoutLogger class is used.
 *
 *  \tparam LoggedDurationType Duration as which the timer's state is logged
 * (optional). If not specifed, the timer's internal Duration is used.
 *
 *  \param timer Timer instance to be logged
 *  \param outputStream Reference to a std::ostream instance, defaults to
 * std::cout
 */
template <class LoggedDurationType = void, class TimerType>
void log(TimerType const &timer, std::ostream &outputStream = std::cout)
{
    using Ldt
        = std::conditional_t<std::is_same_v<LoggedDurationType, void>,
                             typename TimerType::Duration, LoggedDurationType>;

    log(timer, detail::StreamLogger<TimerType, Ldt>{outputStream});
}


////////// Timer classes ///////////////////////////////////////////////////////

/** Timer base class
 *
 * This is the base class for all timers, which are derived using the CRTP
 * pattern.
 *
 * \tparam Derived Derived timer type
 * \tparam Policies List of policies, see \ref policy_group (optional)
 */
template <typename Derived, typename... Policies>
class Timer : public detail::TimerPolicyList<Policies...>::NamePolicyType
{
public:
    using Clock =
        typename detail::TimerPolicyList<Policies...>::ClockPolicyType::Clock;
    using NamePolicy =
        typename detail::TimerPolicyList<Policies...>::NamePolicyType;

    /** The timer's duration type */
    using Duration = typename Clock::duration;

    /** Start measurement cycle */
    void tic()
    {
        this->startTime = Clock::now();
        static_cast<Derived &>(*this).ticImpl();
    }

    /** Stop measurement cycle
     *
     * \tparam OutputDurationType May be used to specifiy a type of the returned
     * duration that is different to Timer::Duration (optional)
     *
     * \returns The duration measured by this call
     */
    template <typename OutputDurationType = Duration>
    OutputDurationType toc()
    {
        return static_cast<Derived &>(*this)
            .template tocImpl<OutputDurationType>();
    }

    /** Get the duration of the latest sample
     *
     * Equivalent to retrieving the return value of toc().
     * This method may be used to access the latest sample after toc() was
     * called.
     *
     * \tparam OutputDurationType May be used to specifiy a type of the returned
     * duration that is different to Timer::Duration (optional)
     *
     * \returns Latest measured duration
     */
    template <typename OutputDurationType = Duration>
    OutputDurationType getLatestDuration() const
    {
        return std::chrono::duration_cast<OutputDurationType>(
            this->latestSample);
    }

protected:
    typename Clock::time_point startTime{};
    Duration                   latestSample{0};
};

/** Single step measurement Timer
 *
 * Timer for single measurements. Successive calls to toc() will prolong the
 * measurement period. This behavior may be used to retrieve multiple durations
 * relative to a single start point.
 *
 * \tparam Policies List of policies, see \ref policy_group (optional)
 */
template <typename... Policies>
class SingleStepTimer
: public Timer<SingleStepTimer<Policies...>,
               typename detail::TimerPolicyList<Policies...>::ClockPolicyType,
               typename detail::TimerPolicyList<Policies...>::NamePolicyType>
{
    using BaseTimer
        = Timer<SingleStepTimer<Policies...>,
                typename detail::TimerPolicyList<Policies...>::ClockPolicyType,
                typename detail::TimerPolicyList<Policies...>::NamePolicyType>;

public:
    using Clock    = typename BaseTimer::Clock;
    using Duration = typename BaseTimer::Duration;

private:
    friend BaseTimer;
    void ticImpl() {}

    template <typename OutputDurationType = Duration>
    auto tocImpl()
    {
        this->latestSample = Clock::now() - this->startTime;
        return std::chrono::duration_cast<OutputDurationType>(
            this->latestSample);
    }
};

/** Base class for multiple-step timers
 *
 * MultipleStepTimers can be used to sample the latency of a code section
 * multiple times. Implementations provide statistics that can be accessed using
 * getStatistics() and which are updated on each call to toc().
 *
 * \tparam Policies List of policies, see \ref policy_group (optional)
 */
template <typename Derived, typename... Policies>
class MultipleStepTimer
: public Timer<MultipleStepTimer<Derived, Policies...>, Policies...>
{
    using BaseTimer
        = Timer<MultipleStepTimer<Derived, Policies...>, Policies...>;

public:
    using Clock    = typename BaseTimer::Clock;
    using Duration = typename BaseTimer::Duration;

    /** Get measurement statistics
     *
     * \tparam OutputDurationType May be used to specifiy a type of the returned
     * duration that is different to Timer::Duration (optional)
     */
    template <typename OutputDurationType = Duration>
    Statistics<OutputDurationType> getStatistics() const
    {
        Statistics<OutputDurationType> output;

        output.minDuration = std::chrono::duration_cast<OutputDurationType>(
            this->statistics.minDuration);
        output.maxDuration = std::chrono::duration_cast<OutputDurationType>(
            this->statistics.maxDuration);
        output.meanDuration = std::chrono::duration_cast<OutputDurationType>(
            static_cast<Derived const &>(*this).getMeanDuration());

        return output;
    }

protected:
    friend BaseTimer;

    void ticImpl() { static_cast<Derived &>(*this).onTic(); }

    template <typename OutputDurationType = Duration>
    auto tocImpl()
    {
        this->latestSample = Clock::now() - this->startTime;

        static_cast<Derived &>(*this).onToc();

        this->updateStatistics(this->latestSample);

        return std::chrono::duration_cast<OutputDurationType>(
            this->latestSample);
    }

    using internalStatisticsT = Statistics<typename BaseTimer::Duration>;
    internalStatisticsT statistics;

    void updateStatistics(const Duration durDelta)
    {
        this->statistics.minDuration = (this->statistics.minDuration > durDelta)
                                           ? durDelta
                                           : this->statistics.minDuration;
        this->statistics.maxDuration = (this->statistics.maxDuration < durDelta)
                                           ? durDelta
                                           : this->statistics.maxDuration;
    };
};

/** Buffering timer calculating the mean duration as a moving average
 *
 *  Calculates the mean value over samples in a ring buffer.
 *
 * \tparam WindowLength    Length of the ring buffer
 * \tparam Policies List of policies, see \ref policy_group (optional)
 */
template <size_t WindowLength, typename... Policies>
class MovingAverageTimer
: public MultipleStepTimer<MovingAverageTimer<WindowLength, Policies...>,
                           Policies...>
{
    using BaseTimer
        = MultipleStepTimer<MovingAverageTimer<WindowLength, Policies...>,
                            Policies...>;

public:
    using Clock    = typename BaseTimer::Clock;
    using Duration = typename BaseTimer::Duration;

private:
    friend BaseTimer;

    void onTic() {};

    void onToc()
    {
        this->period = this->movingAverage.step(this->latestSample);

        this->updateStatistics(this->latestSample);
    }

    Duration getMeanDuration() const { return this->period; };

    using MovingAverage
        = detail::MovingAverage<WindowLength, typename BaseTimer::Duration>;
    MovingAverage movingAverage;

    Duration period{0};
};

/** Non-buffering MultipleStepTimer calculating an average duration
 *
 *  This implementation sums up the overall duration and number of iterations.
 * It does not handle potential overflows yet.
 *
 * \tparam Policies List of policies, see \ref policy_group (optional)
 */
template <typename... Policies>
class AveragingTimer
: public MultipleStepTimer<AveragingTimer<Policies...>, Policies...>
{
    using BaseTimer
        = MultipleStepTimer<AveragingTimer<Policies...>, Policies...>;

public:
    using Clock    = typename BaseTimer::Clock;
    using Duration = typename BaseTimer::Duration;

private:
    friend BaseTimer;

    void onTic() {};

    void onToc()
    {
        sum += this->latestSample;
        this->period = sum / ++nIterations;
    }

    Duration getMeanDuration() const { return this->period; };

    Duration sum{0};
    int64_t  nIterations{0};
    Duration period{0};
};

} // namespace tck

#if __cplusplus < 202002L
template <typename Rep, typename Period>
inline std::ostream& tck::operator<<(
                         std::ostream &os,
                         const std::chrono::duration<Rep, Period> d)
{
    os << d.count() << detail::DurationUnit<Period>::value;
    return os;
}
#endif

template <typename Rep, typename Period>
inline std::ostream &
tck::operator<<(std::ostream &os,
           Statistics<std::chrono::duration<Rep, Period>> const &s)
{
    os << "mean = " << s.meanDuration << ", [min, max] = [" << s.minDuration
       << ", " << s.maxDuration << "]";

    return os;
}

#endif // TCK_TIMER_H
