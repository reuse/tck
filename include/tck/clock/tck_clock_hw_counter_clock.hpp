/** \file tck_clock_hw_counter_clock.hpp
 *
 * \brief Provides a clock based on a HW (CPU) counter.
 *
 * Note that this clock introduces portability issues
 * and is prone to several platfrom-dependent pitfalls.
 *
 * So far, this is only available for
 *
 * - x64: Uses the Timestamp Counter (TSC)
 * - arm64: Virtual timer (CNTVCT)
 */

#ifndef TCK_CLOCK_HW_COUNTER_CLOCK
#define TCK_CLOCK_HW_COUNTER_CLOCK

#include <chrono>
#include <ratio>
#include <utility>

#if defined __GNUG__
#ifdef __x86_64__
#include <cpuid.h>
#include <x86intrin.h>
#elif __aarch64__
#else
static_assert(false, "Platform not supported");
#endif
#else
static_assert(false, "Compiler not supported");
#endif

namespace tck::clock
{

namespace detail
{
#ifdef __x86_64__

enum class HwCounterInfo : int
{
    CpuidNotAvailable           = -1,
    MissingData                 = -2,
    CalculatedByCoreCounterFreq = 1,
    CalculatedByProcBaseFreq    = 2,
};

int64_t tscFreqFromCoreClockFreq();
int64_t tscFreqFromProcessorBaseFreq();
#endif
} // namespace detail

/** Utilities for HwCounterClocks */
struct HwCounterClockUtils
{

    /** Determines the frequency of the HW counter
     *
     * This function attempts to determine the HW counter frequency by reading
     * cpuid information. Depending on the processor model and configuration,
     * either TSC and core crystal clock frequencies are evaluated (int return
     * value is 1), or the TSC frequency matches the processor's base frequency
     * (int return value is 2).
     *
     * \returns std::pair holding the HW counter frequency in Hz (1st elem)
     *          and an integer (2nd elem) indicating the method by which the
     *          frequency was obtained.
     *          On error, the 2nd element holds the values
     *          - -1: cpuid information not available
     *          - -2: cpuid returned insufficient data
     */
    static std::pair<int64_t, int> getHwCounterFrequency();
};

/** Clock based on a HW counter
 *
 * std::chrono-compatible clock based on a HW (CPU) counter.
 * This implementation does not include serializing instructions
 * around the counter access.
 *
 * \tparam CounterFreq Frequency of the HW counter
 */
template <int64_t CounterFreq>
class HwCounterClock
{
public:
    // Definitions to fulfill TrivalClock requirements
    using rep      = int64_t;
    using period   = std::ratio<1, CounterFreq>;
    using duration = std::chrono::duration<rep, period>;

    using time_point = std::chrono::time_point<HwCounterClock>;

    static constexpr bool is_steady{true};

    static time_point now();
};

////////// Inline defintions ///////////////////////////////////////////////////
#ifdef __x86_64__
inline int64_t detail::tscFreqFromCoreClockFreq()
{
    using namespace detail;

    constexpr unsigned int TSC_AND_CORE_FREQ_LEAF{0x15};

    // Ratio TSC/Core freq
    unsigned int ratioDen{0}; // eax
    unsigned int ratioNum{0}; // ebx
    unsigned int coreFreq{0}; // ecx
    unsigned int edx{0};      // reserved

    const int leafSupported = __get_cpuid(TSC_AND_CORE_FREQ_LEAF, &ratioDen,
                                          &ratioNum, &coreFreq, &edx);

    if (leafSupported == 0 || coreFreq == 0)
    {
        return static_cast<int64_t>(HwCounterInfo::CpuidNotAvailable);
    }
    else if (ratioNum == 0 || ratioDen == 0)
    {
        return static_cast<int64_t>(HwCounterInfo::MissingData);
    }
    else
    {
        // Calculate TSC freq based on TSC/CORE freq leaf
        return static_cast<int64_t>(coreFreq * static_cast<double>(ratioNum)
                                    / ratioDen);
    }
}

inline int64_t detail::tscFreqFromProcessorBaseFreq()
{
    using namespace detail;

    constexpr unsigned int PROC_AND_BUS_FREQ_LEAF{0x16};

    unsigned int procBaseFreq{0}; // eax
    unsigned int procMaxFreq{0};  // ebx
    unsigned int busFreq{0};      // ecx
    unsigned int edx{0};          // reserved

    const int leafSupported = __get_cpuid(PROC_AND_BUS_FREQ_LEAF, &procBaseFreq,
                                          &procMaxFreq, &busFreq, &edx);

    if (leafSupported == 0)
    {
        return static_cast<int64_t>(HwCounterInfo::CpuidNotAvailable);
    }
    else if (procBaseFreq == 0)
    {
        return static_cast<int64_t>(HwCounterInfo::MissingData);
    }
    else
    {
        return static_cast<int64_t>(procBaseFreq * 1'000'000UL);
    }
}
#endif

inline std::pair<int64_t, int> HwCounterClockUtils::getHwCounterFrequency()
{
    using namespace detail;

    uint64_t hwCounterFreq{0};
    int      srcId{-1};

#ifdef __x86_64__
    const int64_t fromCoreClockFreqResult = tscFreqFromCoreClockFreq();

    if (fromCoreClockFreqResult < 0)
    {
        const int64_t fromProcBaseFreqResult = tscFreqFromProcessorBaseFreq();

        if (fromProcBaseFreqResult < 0)
        {
            srcId = static_cast<int>(fromProcBaseFreqResult);
        }
        else
        {
            hwCounterFreq = fromProcBaseFreqResult;
            srcId = static_cast<int>(HwCounterInfo::CalculatedByProcBaseFreq);
        }
    }
    else
    {
        hwCounterFreq = fromCoreClockFreqResult;
        srcId = static_cast<int>(HwCounterInfo::CalculatedByCoreCounterFreq);
    }
#elif __aarch64__
    asm volatile("mrs %0, cntfrq_el0; isb; " : "=r"(hwCounterFreq)::"memory");
    // FIXME Extend srcId
#endif

    return std::make_pair(hwCounterFreq, srcId);
}

template <int64_t CounterFreq>
inline typename HwCounterClock<CounterFreq>::time_point
HwCounterClock<CounterFreq>::now()
{
    rep t{0};
#ifdef __x86_64__
    // TODO Include serializing instructions?
    t = static_cast<rep>(__rdtsc());
#elif __aarch64__
    asm volatile("mrs %0, cntvct_el0" : "=r"(t));
#endif

    return time_point{duration{t}};
}

} // namespace tck::clock

#endif // TCK_CLOCK_HW_COUNTER_CLOCK
