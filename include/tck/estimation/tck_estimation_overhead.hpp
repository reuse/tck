#pragma once

#include "tck/clock/tck_clock_hw_counter_clock.hpp"
#include <algorithm>
#include <cassert>
#include <chrono>
#include <cmath>
#include <iostream>
#include <numeric>
#include <stdexcept>
#include <vector>

namespace tck
{
namespace estimation
{

// Typedefs
using OverheadDurationType = std::chrono::nanoseconds;

/** Result of the overhead estimation for a timer */
struct OverheadEstimationResult
{
    OverheadDurationType min{0};
    OverheadDurationType max{0};
    OverheadDurationType mean{0};
    OverheadDurationType stddev{0};
};

namespace detail
{

#if defined TCK_ESTIMATION_USE_TEST_CLOCK
using EstimationClockType = tck::test::TestClock;
#elif defined TCK_ESTIMATION_USE_HW_COUNTER_CLOCK
using EstimationClockType = tck::clock::HwCounterClock<1'000'000'000>;
#else
using EstimationClockType = std::chrono::steady_clock;
#endif

[[nodiscard]] OverheadEstimationResult
getStatistics(std::vector<OverheadDurationType> const &samples)
{
    assert(samples.size() > 0);

    const auto [min, max] = std::minmax_element(cbegin(samples), cend(samples));

    const auto sum = std::reduce(cbegin(samples), cend(samples),
                                 OverheadDurationType{0}, std::plus<>());

    const auto mean = sum / static_cast<long int>(samples.size());

    const auto var
        = std::accumulate(
              cbegin(samples), cend(samples), OverheadDurationType{0},
              [&mean](const auto a, const auto b)
              {
                  const auto d = b - mean;

                  const auto d2 = d.count() * d.count();
                  return a
                         + OverheadDurationType{OverheadDurationType::rep{d2}};
              })
          / (samples.size() - 1);

    const auto stddev = OverheadDurationType{
        static_cast<OverheadDurationType::rep>(sqrt(var.count()))};

    return {*min, *max, mean, stddev};
}

} // namespace detail

/** Class to perform an overhead estimation for a given TimerType */
template <class TimerType>
class OverheadEstimation
{
public:
    /** Create an OverheadEstimation for TimerType
     *
     * If compiled with TCK_ESTIMATION_USE_HW_COUNTER_CLOCK,
     * the constructor will try to determine the frequency of
     * the HW counter used for measurement.
     */
    OverheadEstimation();

    /** Runs the estimation for TimerType */
    template <class SampleFunction>
    [[nodiscard]] OverheadEstimationResult run(const int      nSamples,
                                               SampleFunction sampleFunction);

    /** Retrieve the complete set of samples */
    [[nodiscard]] std::vector<OverheadDurationType> const &getSamples() const;

private:
    [[nodiscard]] OverheadDurationType
    ToOverheadDurationType(detail::EstimationClockType::duration duration);

    template <class SampleFunction>
    [[nodiscard]] std::pair<OverheadDurationType, typename TimerType::Duration>
    takeSample(TimerType &timer, SampleFunction f);

    template <class SampleFunction>
    [[nodiscard]] std::vector<OverheadDurationType>
    sampleOverheadForTimer(const int nSamples, SampleFunction sampleFunction);

    std::vector<OverheadDurationType> m_samples;
    int64_t m_samplingClockFreq{0};
};

/** Output stream operator overload for OverheadEstimationResult */
std::ostream &operator<<(std::ostream &os, OverheadEstimationResult const &res)
{
    os << "tck overhead estimation:"
       << " range = [" << res.min.count() << ", " << res.max.count() << "] ns,"
       << " mean = " << res.mean.count() << "ns,"
       << " stddev = " << res.stddev.count() << "ns";

    return os;
}

////////// Inline function definitions /////////////////////////////////////////
template<class TimerType>
inline OverheadEstimation<TimerType>::OverheadEstimation()
{
#ifdef TCK_ESTIMATION_USE_HW_COUNTER_CLOCK
    using namespace clock;
    const auto [freq, srcId] = HwCounterClockUtils::getHwCounterFrequency();

    if(srcId < 0 || freq == 0)
    {
        throw std::runtime_error("Unable to determine frequency of sampling clock");
    }

    m_samplingClockFreq = freq;
#endif
}

template<class TimerType>
template <class SampleFunction>
inline OverheadEstimationResult OverheadEstimation<TimerType>::run(const int nSamples,
                                    SampleFunction sampleFunction)
{
#ifdef TCK_ESTIMATION_USE_HW_COUNTER_CLOCK
    assert(m_samplingClockFreq > 0);
#endif

    if (nSamples <= 0)
    {
        throw std::invalid_argument(
            "OverheadEstimation::run requires a positive argument value");
    }

    m_samples = this->sampleOverheadForTimer(nSamples, sampleFunction);

    return detail::getStatistics(m_samples);
}

template<class TimerType>
inline std::vector<OverheadDurationType> const & OverheadEstimation<TimerType>::getSamples() const
{
    return m_samples;
}

template <class TimerType>
inline OverheadDurationType OverheadEstimation<TimerType>::ToOverheadDurationType(detail::EstimationClockType::duration duration)
{
#ifdef TCK_ESTIMATION_USE_HW_COUNTER_CLOCK
    constexpr int HW_COUNTER_CONV_SCALE{1000};
    const auto d_ = (HW_COUNTER_CONV_SCALE*OverheadDurationType::period::den/m_samplingClockFreq)*duration.count();
    return OverheadDurationType{d_/HW_COUNTER_CONV_SCALE};
#else
    return std::chrono::duration_cast<OverheadDurationType>(duration);
#endif
}

template <class TimerType>
template <class SampleFunction>
std::pair<OverheadDurationType, typename TimerType::Duration>
OverheadEstimation<TimerType>::takeSample(TimerType &timer, SampleFunction f)
{
    using namespace detail;

    const auto outerStart = EstimationClockType::now();
    timer.tic();
    const auto innerStart = EstimationClockType::now();

    [[maybe_unused]] volatile auto _ = f();

    const auto innerDuration = EstimationClockType::now() - innerStart;
    auto       timerDuration = timer.toc();
    const auto outerDuration = EstimationClockType::now() - outerStart;

    return {ToOverheadDurationType(outerDuration - innerDuration), timerDuration};
}

template <class TimerType>
template <class SampleFunction>
inline std::vector<OverheadDurationType>
OverheadEstimation<TimerType>::sampleOverheadForTimer(const int nSamples, SampleFunction sampleFunction)
{
    assert(nSamples > 0);

    TimerType timer;

    std::vector<std::chrono::nanoseconds> samples;

    int cnt{0};
    while (cnt < nSamples)
    {
        ++cnt;
        const auto [overheadDuration, fcnDuration]
            = this->takeSample(timer, sampleFunction);
        samples.push_back(overheadDuration);
    }

    return samples;
}
} // namespace estimation
} // namespace tck
