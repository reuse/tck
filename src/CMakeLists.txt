
cmake_minimum_required(VERSION 3.13)

add_library(
    "${PROJECT_NAME}Impl"
    tck/tck.cpp
    tck/clock/hw_counter_clock.cpp
    tck/estimation/overhead.cpp
)

target_compile_options("${PROJECT_NAME}Impl" PRIVATE -Wall -Wextra -Werror -Wstrict-overflow -pedantic)

target_link_libraries(
  "${PROJECT_NAME}Impl"
  PUBLIC
  "${PROJECT_NAME}"
)
