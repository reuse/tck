
#include "tck/tck.hpp"
#include "tck/estimation/tck_estimation_overhead.hpp"

#include <algorithm>
#include <cxxabi.h>
#include <memory>
#include <typeinfo>
#include <vector>

namespace
{

using namespace tck;
using namespace tck::estimation;

template <typename T>
class DemangledTypeName
{
public:
    [[nodiscard]] std::string Get() const
    {
        if (!d)
        {
            const std::string msg
                = "Demangling '" + std::string(typeid(T).name())
                  + "' failed with status code " + std::to_string(this->status);
            throw std::runtime_error(msg);
        }

        return d.get();
    }

private:
    int status{0};

    std::unique_ptr<char, decltype(std::free) *> d{
        abi::__cxa_demangle(typeid(T).name(), nullptr, nullptr, &this->status),
        std::free};
};

struct Dummy
{
    int run()
    {
        std::generate(begin(m_data), end(m_data),
                      [d = 0]() mutable { return d++; });

        return std::accumulate(cbegin(m_data), cend(m_data), 0);
    }

    static constexpr size_t N_DUMMY_SAMPLES{8192};
    std::vector<int>        m_data = std::vector<int>(N_DUMMY_SAMPLES);
};

template <typename TimerType>
void estimateOverhead(const int nSamples)
{
    std::cout << "Performing overhead estimation for timer type '"
              << DemangledTypeName<TimerType>().Get() << "'...\n";

    OverheadEstimation<TimerType> estimation;
    Dummy                         dummy;

    const auto result
        = estimation.run(nSamples, [&dummy]() { return dummy.run(); });

    std::cout << "  Result: " << result << "\n";
}

template <typename... TimerTypes>
struct TimerTypeList
{};

template <typename... TimerTypes>
void estimateForEach(TimerTypeList<TimerTypes...> /*unused*/,
                     const int nSamples)
{
    (estimateOverhead<TimerTypes>(nSamples), ...);
}

} // namespace

int main(int argc, char **argv)
{
    using EstimateableTimerTypes
        = TimerTypeList<tck::SingleStepTimer<>, tck::AveragingTimer<>,
                        tck::MovingAverageTimer<1024>,
                        tck::MovingAverageTimer<8192>,
                        tck::MovingAverageTimer<
                            16384>>; // NOLINT(readability-magic-numbers,
                                     // cppcoreguidelines-avoid-magic-numbers)

    std::cout << "Running " << argv[0] << " (tck version " << TCK_VERSION
              << ")...\n";
    try
    {
        constexpr int N_WARMUP_SAMPLES{1000};
        constexpr int N_ESTIMATION_SAMPLES{100'000};

        // Warm up
        // FIXME Probably should warm up for each type
        estimateOverhead<tck::SingleStepTimer<>>(N_WARMUP_SAMPLES);

        // Actual estimation
        estimateForEach(EstimateableTimerTypes{}, N_ESTIMATION_SAMPLES);

        return 0;
    }
    catch (std::exception &e)
    {
        std::cerr << "Error running overhead estimation: " << e.what() << '\n';
    }
    catch (...)
    {
        std::cerr << "Unknown error running overhead estimation\n";
    }

    return -1;
}
