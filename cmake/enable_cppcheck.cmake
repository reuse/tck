
find_program(CPPCHECK_EXECUTABLE NAMES cppcheck)

if(CPPCHECK_EXECUTABLE)
    set(
        CMAKE_CXX_CPPCHECK
        "${CPPCHECK_EXECUTABLE}"
        "--quiet"
        "--cppcheck-build-dir=${CMAKE_BINARY_DIR}"
        "--enable=warning,style,performance,portability,information"
        "--suppressions-list=${CMAKE_SOURCE_DIR}/cppcheck.suppressions"
        "--checkers-report=${CMAKE_BINARY_DIR}/checkers_report.txt"
    )
  message(STATUS "Enabled cppcheck for static analysis (${CMAKE_CXX_CPPCHECK})")
else()
  message(WARNING "cppcheck not found")
endif()
