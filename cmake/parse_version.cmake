
function(parse_version fileName  variableName versionResult)

    set(versionRegex ".*${variableName}.*[ \t]*=*[ \t]*\"([-\.A-Za-z0-9]+)\"")
    
    file(STRINGS "${fileName}" versionMatch REGEX ${versionRegex})

    if("${versionMatch}" STREQUAL "")
      message(FATAL_ERROR "Project version string was not found in \"${fileName}\"")
    endif()

    string(REGEX REPLACE ${versionRegex} "\\1" versionMatch ${versionMatch})

    set("${versionResult}" ${versionMatch} PARENT_SCOPE)

endfunction()
