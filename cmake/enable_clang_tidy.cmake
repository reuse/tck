
find_program(CLANG_TIDY_EXECUTABLE NAMES clang-tidy)

if(CLANG_TIDY_EXECUTABLE)
    set(
      CMAKE_CXX_CLANG_TIDY
      "${CLANG_TIDY_EXECUTABLE}"
      "-p=${CMAKE_BINARY_DIR}"   # Path to compilation database
      "--extra-arg=-std=c++17"  # Fallback if comp. database does not contain -std for a target
    )
  message(STATUS "Enabled clang-tidy for static analysis (${CMAKE_CXX_CLANG_TIDY})")
else()
  message(WARNING "clang-tidy not found")
endif()
