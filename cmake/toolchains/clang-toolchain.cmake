# Toolchain file to faclitate using clang
#
# This scripts simply sets the required varibales to the expected values.
# If CB_CLANG_VERSION_SUFFIX is set in the environment, the tools must be
# found on the system if specified with suffix.
#
# NOTE:
# A more elaborate approach might use find_program with the regular and
# suffixed names and perform a version check if the tools are found by the
# regular name.

if(DEFINED ENV{CB_CLANG_VERSION_SUFFIX})
  set(VERSION_SUFFIX "-$ENV{CB_CLANG_VERSION_SUFFIX}")
endif()

set(CMAKE_C_COMPILER "clang${VERSION_SUFFIX}")
set(CMAKE_CXX_COMPILER "clang++${VERSION_SUFFIX}")

set (CMAKE_AR      "llvm-ar${VERSION_SUFFIX}")
set (CMAKE_LINKER  "llvm-ld${VERSION_SUFFIX}")
set (CMAKE_NM      "llvm-nm${VERSION_SUFFIX}")
set (CMAKE_OBJDUMP "llvm-objdump${VERSION_SUFFIX}")
set (CMAKE_RANLIB  "llvm-ranlib${VERSION_SUFFIX}")
