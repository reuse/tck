# Toolchain file to faclitate cross-compiling for ARM64 Linux (musl)

set(CMAKE_SYSTEM_NAME Linux)

set(CB_TARGET_PREFIX "aarch64-linux-musl")

set(CMAKE_C_COMPILER "${CB_TARGET_PREFIX}-gcc")
set(CMAKE_CXX_COMPILER "${CB_TARGET_PREFIX}-g++")

set (CMAKE_AR      "${CB_TARGET_PREFIX}-ar")
set (CMAKE_LINKER  "${CB_TARGET_PREFIX}-ld")
set (CMAKE_NM      "${CB_TARGET_PREFIX}-nm")
set (CMAKE_OBJDUMP "${CB_TARGET_PREFIX}-objdump")
set (CMAKE_RANLIB  "${CB_TARGET_PREFIX}-ranlib")
set (CMAKE_SIZE    "${CB_TARGET_PREFIX}-size")
set (CMAKE_STRIP   "${CB_TARGET_PREFIX}-strip")

# Modify to point to proper root path of project
# FIXME Does not seem to prevent librarys installed for the host system to be found in all cases
set(CMAKE_FIND_ROOT_PATH "~/opt/aarch64-linux-musl")
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
