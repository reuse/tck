
#include "tck/tck.hpp"
#include "tck/tck_estimation_overhead.hpp"

#include <algorithm>
#include <iostream>
#include <random>
#include <type_traits>

#include <version>

#ifdef __cpp_lib_ranges
#include <ranges>
#define TCK_EXAMPLE1_RANGES_AVAILABLE
#else
#pragma message("WARNING: <ranges> library not available")
#endif

namespace
{

std::vector<int> generateInputs(const int nSamples)
{
    std::random_device randDev;

    std::mt19937                       randEngine{randDev()};
    std::uniform_int_distribution<int> dist{1, 64};

    std::vector<int> inputs(nSamples);
    std::generate(begin(inputs), end(inputs),
                  [&dist, &randEngine]() { return dist(randEngine); });

    return inputs;
}

bool isEven(const int i) { return i % 2 == 0; };
int  square(const int i) { return i * i; };

int rangesFilterSquareSum(const std::vector<int> &inputs)
{
    int sum{0};
#ifdef TCK_EXAMPLE1_RANGES_AVAILABLE
    for (const auto &i :
         inputs | std::views::filter(isEven) | std::views::transform(square))
    {
        sum += i;
    }
#else
    std::cerr << "  WARNING: Program was build without support for ranges. "
                 "Will have improper results\n";
#endif
    return sum;
}

int algoFilterSquareSum(const std::vector<int> &inputs)
{
    std::vector<int> evenNumbers;
    std::vector<int> squaredNumbers;

    std::copy_if(cbegin(inputs), cend(inputs), std::back_inserter(evenNumbers),
                 isEven);

    std::transform(cbegin(evenNumbers), cend(evenNumbers),
                   std::back_inserter(squaredNumbers), square);

    return std::accumulate(cbegin(squaredNumbers), cend(squaredNumbers), 0);
}

int rawLoopFilterSquareSum(const std::vector<int> &inputs)
{
    int sum{0};

    for (const auto i : inputs)
    {
        if (isEven(i))
        {
            sum += square(i);
        }
    }

    return sum;
}

template <typename SampleFunction>
void estimateOverhead(SampleFunction sampleFunction)
{
    std::cout << "Performing overhead estimation...\n";

    tck::estimation::OverheadEstimation<tck::SingleStepTimer<>> estimation;

    const auto result = estimation.run(10'000'000, sampleFunction);

    std::cout << "  Result: " << result << "\n";
}
} // namespace

#define CHECK_RESULTS(first, second)                                           \
    do                                                                         \
    {                                                                          \
        if ((first) != (second))                                               \
        {                                                                      \
            std::cerr << "ERROR - Inconsistent result: " << #first << " and "  \
                      << #second << " do not match (" << (first)               \
                      << " != " << (second) << ")\n";                          \
        }                                                                      \
    } while (0)

int main(int argc, char **argv)
{
    using MicroSecondsFloat_T = std::chrono::duration<double, std::micro>;

    std::cout << "Running tck example 1...\n";

    // Preparations
    const std::vector<int> inputs = generateInputs(10000);

    // Overhead estimation
    // We use the algorithm-based implementation as sample function,
    // it likely creates the most cache pressure.
    const auto inputSubrange
        = std::vector<int>{std::cbegin(inputs), std::cbegin(inputs) + 100};
    estimateOverhead([&inputSubrange]()
                     { return algoFilterSquareSum(inputSubrange); });

    // Evaluate sample functions
    std::cout << "Performing timing measurements...\n";

    tck::SingleStepTimer timer;

    timer.tic();
    const int algoResult = algoFilterSquareSum(inputs);
    timer.toc();

    std::cout << "  <algorithm>-based implementation: ";
    tck::log<MicroSecondsFloat_T>(timer);

    timer.tic();
    const int rangesResult = rangesFilterSquareSum(inputs);
    timer.toc();

    std::cout << "  <ranges>-based implementation: ";
    tck::log<MicroSecondsFloat_T>(timer);

    timer.tic();
    const int rawLoopResult = rawLoopFilterSquareSum(inputs);
    timer.toc();

    std::cout << "  raw-loop-based implementation: ";
    tck::log<MicroSecondsFloat_T>(timer);

    CHECK_RESULTS(algoResult, rangesResult);
    CHECK_RESULTS(algoResult, rawLoopResult);

    std::cout << "Completed tck example 1\n";
    return 0;
}
