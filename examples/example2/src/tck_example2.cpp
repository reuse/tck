
#define TCK_ENABLE_MEASUREMENT
#include "tck/tck.hpp"

#include <algorithm>
#include <iostream>
#include <string_view>
#include <type_traits>
#include <variant>
#include <vector>

namespace
{

class Thing
{
public:
    Thing() = default;

    Thing(const uint64_t id, std::string name, std::string desc)
    : m_id{id}
    , m_name{name}
    , m_description{desc}
    {}

    std::string summarize() const
    {
        return std::to_string(m_id) + ": " + m_name + " - " + m_description;
    }

private:
    uint64_t    m_id;
    std::string m_name;
    std::string m_description;
};

struct Freewheel : public Thing
{
    using Thing::Thing;
};
struct Chair : public Thing
{
    using Thing::Thing;
};
struct Televison : public Thing
{
    using Thing::Thing;
};
struct Pencil : public Thing
{
    using Thing::Thing;
};

using Something  = std::variant<Chair, Freewheel, Televison, Pencil>;
using Somethings = std::vector<Something>;

template <typename T>
class ThingMaker
{
public:
    ThingMaker(uint64_t &id, std::string_view prefix, std::string_view desc)
    : m_id{id}
    , m_prefix{prefix}
    , m_desc{desc}
    {}

    T operator()()
    {
        const auto current_id = m_id++;
        return T{current_id, std::string(m_prefix) + std::to_string(current_id),
                 std::string(m_desc)};
    }

private:
    uint64_t        &m_id;
    std::string_view m_prefix;
    std::string_view m_desc;
};

Somethings MakeThings(const size_t count)
{
    uint64_t   id{0};
    Somethings somethings(count);

    const auto countPerThing = count / 4;

    std::generate_n(std::begin(somethings), countPerThing,
                    ThingMaker<Chair>{id, "CH_", "Cozy"});
    std::generate_n(std::begin(somethings) + countPerThing + 1, countPerThing,
                    ThingMaker<Freewheel>{id, "FW_", "Usually too load"});
    std::generate_n(std::begin(somethings) + 2 * countPerThing + 1,
                    countPerThing, ThingMaker<Televison>{id, "TV_", "Dull"});
    std::generate_n(std::begin(somethings) + 3 * countPerThing + 1,
                    countPerThing, ThingMaker<Pencil>{id, "PCL_", "To draw"});

    return somethings;
}

std::string summarizeSwitched(Something const &thing)
{
    std::string s;

    if (std::holds_alternative<Chair>(thing))
    {
        s = std::get<Chair>(thing).summarize();
    }
    else if (std::holds_alternative<Freewheel>(thing))
    {
        s = std::get<Freewheel>(thing).summarize();
    }
    else if (std::holds_alternative<Televison>(thing))
    {
        s = std::get<Televison>(thing).summarize();
    }
    else if (std::holds_alternative<Pencil>(thing))
    {
        s = std::get<Pencil>(thing).summarize();
    }

    return s;
}
} // namespace

int main(int /* unused */, char **argv)
{

    std::cout << "Running " << argv[0] << "... (Ctrl-C to exit)\n";

    const auto things = MakeThings(10'000'000);

    TCK_INIT_TIMER(tck::MovingAverageTimer, visitDuration, 512);
    TCK_INIT_TIMER(tck::MovingAverageTimer, switchedDuration, 512);

    while (true)
    {
        for (const auto &t : things)
        {
            TCK_SAMPLE_BEGIN(visitDuration);
            [[maybe_unused]] const auto s1
                = std::visit([](const auto &t) { return t.summarize(); }, t);
            TCK_SAMPLE_END(visitDuration);

            TCK_SAMPLE_BEGIN(switchedDuration);
            const auto s2 = summarizeSwitched(t);
            TCK_SAMPLE_END(switchedDuration);
        }

        TCK_LOG_TIMER_STATE(visitDuration);
        TCK_LOG_TIMER_STATE(switchedDuration);
    }

    return 0;
}
