
cmake_minimum_required(VERSION 3.13)

find_package(tck 0.2 REQUIRED)

project(tckExample2 LANGUAGES CXX)

add_executable(${PROJECT_NAME} src/tck_example2.cpp)

target_compile_features(${PROJECT_NAME} PRIVATE cxx_std_20)

target_link_libraries(${PROJECT_NAME} tck::tck)
