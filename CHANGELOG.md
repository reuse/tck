# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

* Added clocks that direclty acces HW counters for x64 and arm64.
* Allow overhead estimation utilities to use the HW counter clocks.
* Added CMake presets file with common configurations

### Changed

* Cleaned up structure of `tck.hpp` for easier extensibility.

### Fixed

* Fixed error on `TCK_SAMPLE_END` when passed a timer reference.

### Removed


## [00.02.01] - 2024-11-21

### Fixed

* Fixed use as nested subproject: `CMakeLists.txt` works if not included as top-level lists file.
* Removed erroneous semicolon in macro definition.
* Declared `TCK_VERSION` inline.


## [00.02.00] - 2024-10-23

### Added

* Logging utilities: `log` functions to log timer state by callable or ostream object.
* Macro-based interface: `TCK_ENABLE_MEASUREMENT`, `TCK_INIT_TIMER`, `TCK_SAMPLE_BEGIN`, `TCK_SAMPLE_END`, `TCK_LOG_TIMER_STATE`.
* Timers can optionally be configured by a list of policy types.
* Timers can be tagged by a static instance name by means of the `NamePolicy` type.

### Changed

* The `getLatestDuration` method and the latest sample data member are moved to the `Timer` base class.
* Clock used for measurement is specified by `ClockPolicy`.


## [0.1.1] - 2024-07-12

### Added

### Changed

### Fixed

* Fixed Example 1, which was not adapted to the API of the 0.1.0 release.
* Fixed missing cast to a specifed `OutputDurationType` for `toc`-calls on `MultipleStepTimer` implementations.

### Removed


## [0.1.0] - 2024-02-12

Initial alpha release

### Added

* Common timer interface for `SingleStepTimer` and `MultipleStepTimer`s
* `MultipleStepTimer` implementations: `AveragingTimer` and `MovingAverageTimer`
* Utilities for overhead estimaton

### Changed

### Fixed

### Removed
