# tck

`tck` aims to provide easy-to-use utilities for latency measurements. It is intended to determine latencies in scenarios that are close to the actual deployment and where other profiling tools may not be practical to use. The library is based on `C++-17` and `std::chrono`. The main functionality is provided in a single header file.

## Usage

All measurement utilities are included in `tck/tck.hpp`.

A timer instance is defined locally in the scope of interest by using `TCK_INIT_TIMER`. The first parameter must be one of the available [timer types](#timers). The second parameter specifies a name of the timer instance. For example
```
TCK_INIT_TIMER(tck::SingleStepTimer, readDuration)
```
defines a `SingleStepTimer` named `readDuration`. Afterwards, the section to be measured may be sampled by `TCK_SAMPLE_BEGIN` and `TCK_SAMPLE_END`, e.g.:
```
TCK_SAMPLE_BEGIN(readDuration);
read();
TCK_SAMPLE_END(readDuration);
```
Measurements are enabled by compiling with `TCK_ENABLE_MEASUREMENT` defined, otherwise the macros will have no effect.

To ease retrieval of measurement results, `TCK_LOG_TIMER_STATE` can be used, see section [Logging](#logging) below.

## Timers

The following timers are provided:

| Timer                        | Description |
|:-----------------------------|:------------|
| `SingleStepTimer` | Timer for single measurements |
| `MovingAverageTimer` | Buffering timer calculating the mean duration as a moving average over `WindowLength` samples |
| `AveragingTimer` | Non-buffering MultipleStepTimer calculating an average duration |

For details, see the [API documentation](#documentation) of the timer types.

All timers are configurable by policies that can optionally be specified as template arguments: 
| Policy | Description | Default |
|:-------|:------------|:--------|
| `ClockPolicy` | Defines the clock used for measurement by the `ClockType` template parameter. The type must fulfill the [TrivialClock](https://en.cppreference.com/w/cpp/named_req/TrivialClock) requirements. |  `ClockType = std::chrono::steady_clock` |
| `NamePolicy` | A name may be provided by passing `NamePolicy` with a `std::string_view` template argument. | Template argument: Unnamed. `TCK_INIT_TIMER`: Name specified by macro argument. |

Timers can be configured by passing additional arguments to `TCK_INIT_TIMER`. Any positional arguments must preceed the optional policy arguments, the latter may be specified in an arbitary order. If not explicitly specified, `TCK_INIT_TIMER` defines a `NamePolicy` according to the name argument of the macro. Examples:
```
// MovingAverageTimer with WindowLength 1024, name is "movAvrgTimer"
TCK_INIT_TIMER(tck::MovingAverageTimer, movAvrgTimer, 1024); 

// AveragingTimer, unnamed, using MyCustomClock as clock type
TCK_INIT_TIMER(tck::AveragingTimer, averagingTimer, tck::NamePolicy<>, tck::ClockPolicy<MyCustomClock>);
```

## Logging

`tck` provides the `TCK_LOG_TIMER_STATE` macro to display or access the current sample value and statistics of a timer. It takes the timer instance and, optionally, a callable or output stream.

### Logging to output streams

The stream loggers write the timer state to a string of the formats
```
<instanceName>: <current sample duration>
<instanceName>: <current sample duration>, mean: <mean duration>, min/max: [<min duration>, <max duration>]
```
for the `SingleStepTimer` and `MultipleStepTimer`s, respectively. The unit of the duration is selected according to the timer's internal duration type.

By default, `TCK_LOG_TIMER_STATE` uses a steam logger that writes to `stdout`, any `std::ostream` can be passed a as second argument:

```
// Write e.g. "readDuration: 4000ns"
TCK_LOG_TIMER_STATE(readDuration);               // to stdout
TCK_LOG_TIMER_STATE(readDuration, logStream);    // to logStream
```

### Logging by callable

As an alternative, a callable may be passed as the second argument. This may be useful in order to pass the measurements on to variables that can be monitored from e.g. a remote system. The signature of the called function must be of the forms
```
void function(<std::chrono::duration type>)                           // or
void function(<tck::Statistics type>)                                 // or
void function(<std::chrono::duration type>, <tck::Statistics type>)
```
and will be passed the timer's current sample duration, statistics, or both. The durations will be converted to the type of the argument and/or the duration that specializes the `tck::statistics` argument.

The example below illustrates how to forward a timer's state to `float` variables for monitoring:

```
using FloatMicroseconds_T = std::chrono::duration<float, std::micro>;
using FloatMicrosecondStats_T = tck::Statistics<FloatMicroseconds_T>;

// Suppose these are published for remote access on your system
float rdCurr {.0f};
float rdMean {.0f};
float rdMin {.0f};
float rdMax {.0f};

// Define logger callable
auto readDurationLogFunction = [rdCurr, rdMean, rdMin, rdMax](
                                   const FloatMicroseconds_T curr,
                                   FloatMicrosecondStats_T const& stats)
{
    rdCurr = curr;
    rdMean = stats.meanDuration;
    rdMin  = stats.minDuration;
    rdMax  = stats.maxDuration;
};

// Section to be measured ...

// Update monitored variables
TCK_LOG_TIMER_STATE(readDuration, readDurationLogFunction);
```

Note: If the name of the timer instane is of interest for logging, the logger object may be templated on the timer type. For timers that are specified with a name via `NamePolicy`, this allows to access the static `getName` method from the logger.

## Documentation

API documentation can be build using the `doc` target, e.g.
```
cmake --build <path/to/binary/dir> --target doc
```
